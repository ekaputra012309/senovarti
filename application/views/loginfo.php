<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm mb-4 border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Data History Log In-Out
                </h4>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped" id="dataTable">
            <thead>
                <tr>
                    <th width="20px">No.</th>
                    <th>Nama User</th>
                    <th>Role</th>
                    <th>Aksi</th>
                    <th>Taggal</th>
                    <th>Waktu</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($log_user) :
                    foreach ($log_user as $luser) :
                        $tgl = $luser['log_time'];
                ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $luser['nama']; ?></td>
                            <td><?= $luser['role']; ?></td>
                            <td><?= $luser['log_aksi']; ?></td>
                            <td><?= longdate_indo(date('Y-m-d', strtotime($tgl))); ?></td>
                            <td><?= date('H:i:s', strtotime($tgl)) ?></td>
                        </tr>
                    <?php endforeach;
                else : ?>
                    <tr>
                        <td colspan="3" class="text-center">History empty</td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
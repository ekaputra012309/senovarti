<?php
if ($judul == 'Barang West') { ?>
    <table width="100%" border="0">
        <tr>
            <th rowspan="2" width="100" align="left"><img style="max-height: 120px; max-width: 120px;" src="<?= base_url('assets/img/avatar/' . $about['foto']) ?>" alt="logo"></th>
            <th>
                <h2 style="text-align:center; vertical-align:top">Laporan <?= $judul ?></h2>

            </th>
            <th rowspan="2" width="100"></th>
        </tr>
        <tr>
            <th><?= $tanggal ?></th>
        </tr>
    </table>
    <table width="100%" id="doc" border="1">
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">ID Trans</th>
            <th rowspan="2">Tanggal</th>
            <th colspan="4">Detail Transaksi</th>
            <th rowspan="2">Sub Total</th>
        </tr>
        <tr>
            <th width="200">Nama Barang</th>
            <th>Qty</th>
            <th>Harga</th>
            <th width="150">Total</th>
        </tr>
        <?php $no = 1;
        $gttl = 0;
        foreach ($data_t as $bw) : ?>
            <tr style="vertical-align: text-top;">
                <td><?= $no++ ?></td>
                <td><?= $bw['id_barang_west'] ?></td>
                <td align="center"><?= $bw['tanggal_west'] ?></td>
                <td colspan="4">
                    <table width="100%" border="0" style="border-collapse: collapse;">
                        <?php $q = $this->db->get_where('barang_west_d', ['id_barang_west' => $bw['id_barang_west']]);
                        $totalRow = $q->num_rows();
                        $rowCount = 0;
                        foreach ($q->result_array() as $v) {
                            $total = $v['jumlah_west'] * $v['harga'];
                            $rowCount++;
                            $isLastRow = $rowCount === $totalRow;
                        ?>
                            <tr <?= $isLastRow ? '' : ' style="border-bottom: 1px solid grey;"' ?>>
                                <td width="200"><?= $v['nama_barang']; ?></td>
                                <td align="center"><?= $v['jumlah_west'] . ' / Kg'; ?></td>
                                <td align="right">@ <?= number_format($v['harga'], 0, ',', '.'); ?></td>
                                <td width="180" style="text-align: right;">@ <?= number_format($total, 0, ',', '.'); ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
                <td style="text-align: right;"><?= number_format($bw['subtotal'], 0, ',', '.'); ?></td>
            </tr>
        <?php $gttl += $bw['subtotal'];
        endforeach; ?>
    </table>
    <br>
    <table width="100%" id="doc">
        <tr>
            <th style="text-align: right;" colspan="7">Grand Total : &nbsp; IDR</th>
            <th style="text-align: right;" width="120"><?= number_format($gttl, 0, ',', '.'); ?></th>
        </tr>
    </table>
<?php } else if ($judul == 'Barang Masuk') { ?>
    <table width="100%" border="0">
        <tr>
            <th rowspan="2" width="100" align="left"><img style="max-height: 120px; max-width: 120px;" src="<?= base_url('assets/img/avatar/' . $about['foto']) ?>" alt="logo"></th>
            <th>
                <h2 style="text-align:center; vertical-align:top">Laporan <?= $judul ?></h2>

            </th>
            <th rowspan="2" width="100"></th>
        </tr>
        <tr>
            <th><?= $tanggal ?></th>
        </tr>
    </table>
    <table width="100%" border="1" style="border-collapse: collapse;">
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">ID BM</th>
            <th rowspan="2">Supplier</th>
            <th rowspan="2">Tanggal</th>
            <th colspan="4">Detail Transaksi</th>
            <th rowspan="2">Sub Total</th>
            <th rowspan="2">Pajak PPN</th>
            <th rowspan="2">#</th>
        </tr>
        <tr>
            <th width="200">Nama Barang</th>
            <th>Qty</th>
            <th>Harga</th>
            <th width="120">Total</th>
        </tr>
        <?php $no = 1;
        $subtotal = 0;
        $gttl = 0;
        $gttl1 = 0;
        foreach ($data_t as $bm) : ?>
            <tr style="vertical-align: text-top;">
                <td><?= $no++ ?></td>
                <td><?= $bm['id_barang_masuk'] ?></td>
                <td><?= $bm['nama_supplier']; ?></td>
                <td><?= $bm['tanggal_masuk'] ?></td>
                <td colspan="4">
                    <table width="100%" border="0" id="doc">
                        <?php $this->db->join('barang b', 'bmd.barang_id = b.id_brg');
                        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
                        $this->db->where('id_barang_masuk', $bm['id_barang_masuk']);
                        $q = $this->db->get('barang_masuk_d bmd');
                        $totalRow = $q->num_rows();
                        $rowCount = 0;
                        foreach ($q->result_array() as $v) {
                            $total = $v['jumlah_masuk'] * $v['harga_masuk'];
                            $rowCount++;
                            $isLastRow = $rowCount === $totalRow;
                        ?>
                            <tr <?= $isLastRow ? '' : ' style="border-bottom: 1px solid grey;"' ?>>
                                <td width="200"><?= $v['nama_barang']; ?></td>
                                <td align="center" width="60"><?= $v['jumlah_masuk'] . ' ' . $v['nama_satuan']; ?></td>
                                <td align="right" width="80">@ <?= number_format($v['harga_masuk'], 0, ',', '.'); ?></td>
                                <td width="120" style="text-align: right;">@ <?= number_format($total, 0, ',', '.'); ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
                <td style="text-align: right;"><?= number_format($bm['total'], 0, ',', '.'); ?></td>
                <td style="text-align: right;"><?= number_format($bm['pajak_bmd'], 0, ',', '.');
                                                $gttl = $bm['pajak_bmd'] + $bm['total']; ?></td>
                <td style="text-align: right;"><?= number_format($gttl, 0, ',', '.'); ?></td>
            </tr>
        <?php $gttl1 += $gttl;
        endforeach; ?>
    </table>
    <br>
    <table width="100%" id="doc">
        <tr>
            <th style="text-align: right;" colspan="7">Grand Total : &nbsp; IDR</th>
            <th style="text-align: right;" width="120"><?= number_format($gttl1, 0, ',', '.'); ?></th>
        </tr>
    </table>
<?php } else { ?>
    <table width="100%" border="0">
        <tr>
            <th rowspan="2" width="100" align="left"><img style="max-height: 120px; max-width: 120px;" src="<?= base_url('assets/img/avatar/' . $about['foto']) ?>" alt="logo"></th>
            <th>
                <h2 style="text-align:center; vertical-align:top">Laporan <?= $judul ?></h2>

            </th>
            <th rowspan="2" width="100"></th>
        </tr>
        <tr>
            <th><?= $tanggal ?></th>
        </tr>
    </table>
    <table width="100%" border="1" style="border-collapse: collapse;">
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">ID BK</th>
            <th rowspan="2">Tanggal</th>
            <th colspan="4">Detail Transaksi</th>
            <th rowspan="2">Sub Total</th>
            <th rowspan="2">Pajak PPN</th>
            <th rowspan="2">#</th>
        </tr>
        <tr>
            <th width="200">Nama Barang</th>
            <th>Qty</th>
            <th>Harga</th>
            <th width="120">Total</th>
        </tr>
        <?php $no = 1;
        $subtotal1 = 0;
        $gttl = 0;
        $gttl1 = 0;
        foreach ($data_t as $bk) : ?>
            <tr style="vertical-align: text-top;">
                <td><?= $no++ ?></td>
                <td><?= $bk['id_barang_keluar'] ?></td>
                <td><?= $bk['tanggal_keluar'] ?></td>
                <td colspan="4">
                    <table width="100%" border="0" id="doc">
                        <?php $this->db->join('barang b', 'bmd.barang_id = b.id_brg');
                        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
                        $this->db->where('id_barang_keluar', $bk['id_barang_keluar']);
                        $q = $this->db->get('barang_keluar_d bmd');
                        $totalRow = $q->num_rows();
                        $rowCount = 0;
                        foreach ($q->result_array() as $v) {
                            $total = $v['jumlah_keluar'] * $v['harga_keluar'];
                            $rowCount++;
                            $isLastRow = $rowCount === $totalRow;
                        ?>
                            <tr <?= $isLastRow ? '' : ' style="border-bottom: 1px solid grey;"' ?>>
                                <td width="200"><?= $v['nama_barang']; ?></td>
                                <td align="center" width="60"><?= $v['jumlah_keluar'] . ' / ' . $v['nama_satuan']; ?></td>
                                <td align="right" width="80">@ <?= number_format($v['harga_keluar'], 0, ',', '.'); ?></td>
                                <td width="120" style="text-align: right;">@ <?= number_format($total, 0, ',', '.'); ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
                <td style="text-align: right;"><?= number_format($bk['total'], 0, ',', '.'); ?></td>
                <td style="text-align: right;"><?= number_format($bk['pajak_bkd'], 0, ',', '.');
                                                $gttl = $bk['pajak_bkd'] + $bk['total']; ?></td>
                <td style="text-align: right;"><?= number_format($gttl, 0, ',', '.'); ?></td>
            </tr>
        <?php $gttl1 += $gttl;
        endforeach; ?>
    </table>
    <br>
    <table width="100%" id="doc">
        <tr>
            <th style="text-align: right;" colspan="7">Grand Total : &nbsp; IDR</th>
            <th style="text-align: right;" width="120"><?= number_format($gttl1, 0, ',', '.'); ?></th>
        </tr>
    </table>
<?php } ?>
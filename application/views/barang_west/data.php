<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Riwayat Data Barang West
                </h4>
            </div>
            <div class="col-auto">
                <a href="<?= base_url('barangwest/add') ?>" class="btn btn-sm btn-primary btn-icon-split">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">
                        Input Barang west
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>No Transaksi</th>
                    <th>Tanggal</th>
                    <th>Sub Total</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($barangwest) :
                    foreach ($barangwest as $bk) :
                ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $bk['id_barang_west']; ?></td>
                            <td><?= $bk['tanggal_west']; ?></td>
                            <td><?= 'Rp ' . number_format($bk['subtotal'], 0, ',', '.'); ?></td>
                            <td>
                                <?php if (is_admin() || is_owner()) : ?>
                                    <a onclick="return confirm('Yakin ingin hapus?')" href="<?= base_url('barangwest/delete/') . $bk['id_barang_west'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-trash"></i></a>
                                <?php endif ?>
                                <button type="button" class="btn btn-info btn-circle btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg-<?= $bk['id_barang_west']; ?>">
                                    <i class="fa fa-eye"></i>
                                </button>
                                <!-- modal info -->
                                <div class="modal fade bd-example-modal-lg-<?= $bk['id_barang_west']; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="ModalinfoBWLabel">Detail Transaksi</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table">
                                                    <tr>
                                                        <th colspan="2">ID Transaksi : <?= $bk['id_barang_west']; ?></th>
                                                        <th colspan="3" class="text-right">Tanggal : <?= $bk['tanggal_west']; ?></th>
                                                    </tr>
                                                </table>
                                                <table class="table">
                                                    <tr>
                                                        <td width="20">No. </td>
                                                        <td>Nama Barang</td>
                                                        <td>Qty</td>
                                                        <td>Harga</td>
                                                        <td class="text-center">Total</td>
                                                    </tr>
                                                    <?php $no = 1;
                                                    $q = $this->db->get_where('barang_west_d', ['id_barang_west' => $bk['id_barang_west']]);
                                                    foreach ($q->result_array() as $v) {
                                                        $total = $v['jumlah_west'] * $v['harga']; ?>
                                                        <tr>
                                                            <td><?= $no++; ?></td>
                                                            <td><?= $v['nama_barang']; ?></td>
                                                            <td><?= $v['jumlah_west'] . ' / Kg'; ?></td>
                                                            <td>Rp <?= number_format($v['harga'], 0, ',', '.'); ?></td>
                                                            <td class="text-right">Rp <?= number_format($total, 0, ',', '.'); ?></td>
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                    <tr>
                                                        <th colspan="4" class="text-right">Sub Total</th>
                                                        <th class="text-right">Rp <?= number_format($bk['subtotal'], 0, ',', '.'); ?></th>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="7" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
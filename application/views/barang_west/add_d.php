<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Add Barang Sisa
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('barangwest/add') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open('', [], ['id_barang_west' => $id_barang_west]); ?>
                <div class="modal-body">
                    <div class="row form-group">
                        <label class="col-md-4 text-md-right" for="nama_barang">Nama Barang</label>
                        <div class="col-md-6">
                            <input value="<?= set_value('nama_barang'); ?>" name="nama_barang" id="nama_barang" type="text" class="form-control" placeholder="Nama Barang...">
                            <?= form_error('nama_barang', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-md-4 text-md-right" for="jumlah_west">Jumlah (Kg)</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <input value="<?= set_value('jumlah_west'); ?>" name="jumlah_west" id="jumlah_west" type="number" class="form-control" placeholder="Jumlah West...">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="satuan">Kg</span>
                                </div>
                            </div>
                            <?= form_error('jumlah_west', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-md-4 text-md-right" for="harga">Harga Satuan</label>
                        <div class="col-md-5">
                            <input value="<?= set_value('harga'); ?>" name="harga" id="harga" type="number" class="form-control" placeholder="Harga...">
                            <?= form_error('harga', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary">Reset</button>
                    <button type="submit" class="btn btn-primary">Add Barang</button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        // Disable the submit button initially
        $('button[type="submit"]').prop('disabled', true);

        // Check the form fields on keyup event
        $('input[type="text"], input[type="number"]').keyup(function() {
            var empty = false;

            // Check each form field for empty value
            $('input[type="text"], input[type="number"]').each(function() {
                if ($(this).val() === '') {
                    empty = true;
                    return false; // Exit the loop if any field is empty
                }
            });

            // Enable or disable the submit button based on the empty flag
            if (empty) {
                $('button[type="submit"]').prop('disabled', true);
            } else {
                $('button[type="submit"]').prop('disabled', false);
            }
        });
    });
</script>
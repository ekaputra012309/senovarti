<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Input Barang Sisa
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('barangwest') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open('barangwest/add_p', [], ['id_barang_west' => $id_barang_west]); ?>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="">ID Transaksi Barang West</label>
                    <div class="col-md-4">
                        <input value="<?= $id_barang_west; ?>" type="text" readonly="readonly" class="form-control">
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="tanggal_west">Tanggal</label>
                    <div class="col-md-4">
                        <input value="<?= set_value('tanggal_west', date('Y-m-d')); ?>" name="tanggal_west" id="tanggal_west" type="text" class="form-control date" placeholder="Tanggal Masuk...">
                        <?= form_error('tanggal_west', '<small class="text-danger">', '</small>'); ?>
                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-primary" href="<?= base_url('barangwest/add_d') ?>">
                            <i class="fas fa-plus fa-sm fa-fw mr-2 text-gray-400"></i>
                            Add Barang
                        </a>
                    </div>
                </div>
                <div class="row form-group">
                    <table class="table table-striped w-100 dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th>No. </th>
                                <th>Nama Barang</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $subtotal = 0;
                            if ($barangwest_d) :
                                foreach ($barangwest_d as $bk) :
                                    $total = $bk['jumlah_west'] * $bk['harga'];
                            ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $bk['nama_barang']; ?></td>
                                        <td><?= $bk['jumlah_west']; ?></td>
                                        <td><?= $bk['harga']; ?></td>
                                        <td><?= $total; ?></td>
                                        <td>
                                            <a href="<?= base_url('barangwest/delete_d/') . $bk['id'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php $subtotal += $total;
                                endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td colspan="6" class="text-center">
                                        Data Kosong
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 offset-md-4 text-md-right" for="barang_id">Sub Total</label>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input readonly value="<?= set_value('sub_total', $subtotal); ?>" name="subtotal" id="sub_total" type="text" class="form-control" placeholder="Sub Total...">
                        </div>
                        <?= form_error('id_barang', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col offset-md-8">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
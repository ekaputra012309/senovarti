<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Riwayat Data Purchase Order
                </h4>
            </div>
            <div class="col-auto">
                <a href="<?= base_url('purchaseorder/add') ?>" class="btn btn-sm btn-primary btn-icon-split">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">
                        Input Purchase Order
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>No PO</th>
                    <th>Tanggal</th>
                    <th>Supplier</th>
                    <th>Jumlah Tagihan</th>
                    <th>Tgl Brg Datang</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($datapo) :
                    foreach ($datapo as $po) :
                        $noinv = explode('-', $po['no_po']);
                ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $noinv[0] . '/' . $noinv[1] . '/' . $noinv[2] ?></td>
                            <td><?= $po['tanggal_']; ?></td>
                            <td><?= $po['nama_supplier'] ?></td>
                            <td><?= "Rp " . number_format($po['jumlah_tertagih'], 0, ',', '.') ?></td>
                            <td><?= $po['tanggal_brgmasuk']; ?></td>
                            <td>
                                <a href="<?= base_url('purchaseorder/toggle/') . $po['no_po'] ?>" class="btn btn-sm <?= $po['status'] ? 'btn-success' : 'btn-secondary' ?>" title="<?= $po['status'] ? 'Ubah Status ke On Process' : 'Ubah Status ke Confirmed' ?>"><?= $po['status'] ? 'Confirmed' : 'On Process' ?></i></a>
                            </td>
                            <td>
                                <a href="<?= base_url('purchaseorder/cetak/') . $po['no_po'] ?>" target="_blank" class="btn btn-sm btn-info btn-circle "><i class="fa fa-print"></i></a>
                                <a onclick="return confirm('Yakin ingin menghapus data?')" href="<?= base_url('purchaseorder/delete/') . $po['no_po'] ?>" class="btn btn-circle btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="8" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
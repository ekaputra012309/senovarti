<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Riwayat Data Barang Masuk
                </h4>
            </div>
            <div class="col-auto">
                <a href="<?= base_url('barangmasuk/add') ?>" class="btn btn-sm btn-primary btn-icon-split">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">
                        Input Barang Masuk
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>No Transaksi</th>
                    <th>Tanggal Masuk</th>
                    <th>Supplier</th>
                    <th>Sub Total</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($barangmasuk) :
                    foreach ($barangmasuk as $bm) :
                ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $bm['id_barang_masuk']; ?></td>
                            <td><?= $bm['tanggal_masuk']; ?></td>
                            <td><?= $bm['nama_supplier']; ?></td>
                            <td>Rp <?= number_format($bm['total'] + $bm['pajak_bmd'], 0, ',', '.'); ?></td>
                            <td>
                                <?php if (is_admin() || is_owner()) : ?>
                                    <a onclick="return confirm('Yakin ingin hapus?')" href="<?= base_url('barangmasuk/delete/') . $bm['id_barang_masuk'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-trash"></i></a>
                                <?php endif ?>
                                <button type="button" class="btn btn-success btn-circle btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg-<?= $bm['id_barang_masuk']; ?>">
                                    <i class="fa fa-eye"></i>
                                </button>
                                <!-- modal info barang masuk -->
                                <div class="modal fade bd-example-modal-lg-<?= $bm['id_barang_masuk']; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="ModalinfoBWLabel">Detail Transaksi</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table">
                                                    <tr>
                                                        <th colspan="3">ID Transaksi : <?= $bm['id_barang_masuk']; ?></th>
                                                        <th colspan="2" class="text-right">Tanggal : <?= $bm['tanggal_masuk']; ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th>Supplier : <?= $bm['nama_supplier']; ?></th>
                                                    </tr>
                                                </table>
                                                <table class="table">
                                                    <tr>
                                                        <td width="20">No. </td>
                                                        <td>Nama Barang</td>
                                                        <td>Qty</td>
                                                        <td>Harga</td>
                                                        <td class="text-center">Total</td>
                                                    </tr>
                                                    <?php
                                                    $no = 1;
                                                    $subtotal = 0;
                                                    $this->db->join('barang b', 'bmd.barang_id = b.id_brg');
                                                    $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
                                                    $this->db->where('id_barang_masuk', $bm['id_barang_masuk']);
                                                    $q = $this->db->get('barang_masuk_d bmd')->result_array();
                                                    foreach ($q as $v) {
                                                        $total = $v['jumlah_masuk'] * $v['harga_masuk'];
                                                    ?>
                                                        <tr>
                                                            <td><?= $no++; ?></td>
                                                            <td><?= $v['nama_barang']; ?></td>
                                                            <td><?= $v['jumlah_masuk'] . ' / ' . $v['nama_satuan']; ?></td>
                                                            <td><?= number_format($v['harga_masuk'], 0, ',', '.'); ?></td>
                                                            <td class="text-right"><?= number_format($total, 0, ',', '.'); ?></td>
                                                        </tr>
                                                    <?php  }
                                                    $gttl = $bm['total'] + $bm['pajak_bmd']; ?>
                                                    <tr>
                                                        <th colspan="2"></th>
                                                        <th>Total</th>
                                                        <th class="">: &nbsp; &nbsp; IDR</th>
                                                        <th class="text-right"><?= number_format($bm['total'], 0, ',', '.'); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2"></th>
                                                        <th>PPN</th>
                                                        <th class="">: &nbsp; &nbsp; IDR</th>
                                                        <th class="text-right"><?= number_format($bm['pajak_bmd'], 0, ',', '.'); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2"></th>
                                                        <th>Grand Total</th>
                                                        <th class="">: &nbsp; &nbsp; IDR</th>
                                                        <th class="text-right"><?= number_format($gttl, 0, ',', '.'); ?></th>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <a target="_blank" href="<?= base_url('barangmasuk/cetak/') . $bm['id_barang_masuk'] ?>" class="btn btn-danger"><i class="fa fa-print"></i> Cetak</a>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="8" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
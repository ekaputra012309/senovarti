<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Input Barang Masuk
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('barangmasuk') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open('barangmasuk/add_p', [], ['id_barang_masuk' => $id_barang_masuk]); ?>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="id_barang_masuk">ID Transaksi Barang Masuk</label>
                    <div class="col-md-4">
                        <input value="<?= $id_barang_masuk; ?>" type="text" readonly="readonly" class="form-control">
                        <?= form_error('id_barang_masuk', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="tanggal_masuk">Tanggal Masuk</label>
                    <div class="col-md-4">
                        <input value="<?= set_value('tanggal_masuk', date('Y-m-d')); ?>" name="tanggal_masuk" id="tanggal_masuk" type="text" class="form-control date" placeholder="Tanggal Masuk...">
                        <?= form_error('tanggal_masuk', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="supplier_id">Supplier</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <select name="supplier_id" id="supplier_id" class="custom-select">
                                <option value="" selected disabled>Pilih Supplier</option>
                                <?php foreach ($supplier as $s) : ?>
                                    <option <?= set_select('supplier_id', $s['id_supplier']) ?> value="<?= $s['id_supplier'] ?>"><?= $s['nama_supplier'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <?= form_error('supplier_id', '<small class="text-danger">', '</small>'); ?>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-primary" href="<?= base_url('barangmasuk/add_d') ?>">
                            <i class="fas fa-plus fa-sm fa-fw mr-2 text-gray-400"></i>
                            Add Barang
                        </a>
                    </div>
                </div>
                <div class="row form-group">
                    <table class="table table-striped w-100 dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th>No. </th>
                                <th>Nama Barang</th>
                                <th>Qty | Satuan</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $subtotal = 0;
                            if ($barangmasuk_d) :
                                foreach ($barangmasuk_d as $bk) :
                                    $total = $bk['jumlah_masuk'] * $bk['harga_masuk'];
                            ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $bk['nama_barang']; ?></td>
                                        <td><?= $bk['jumlah_masuk'] . ' | ' . $bk['nama_satuan']; ?></td>
                                        <td><?= $bk['harga_masuk']; ?></td>
                                        <td><?= $total; ?></td>
                                        <td>
                                            <a href="<?= base_url('barangmasuk/delete_d/') . $bk['id_bmd'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php $subtotal += $total;
                                endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td colspan="7" class="text-center">
                                        Data Kosong
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="row form-group">
                    <label class="col-md-6 text-md-right" for="pajak_bmd">Pajak</label>
                    <div class="col-md-6">
                        <div class="custom-control custom-radio">
                            <input onchange="radioBtn('0')" <?= set_radio('pajakbmd', '0'); ?> value="0" type="radio" id="0" name="pajakbmd" class="custom-control-input">
                            <label class="custom-control-label" for="0">Tidak Ada Pajak</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input onchange="radioBtn('1')" <?= set_radio('pajakbmd', '1'); ?> value="1" type="radio" id="1" name="pajakbmd" class="custom-control-input">
                            <label class="custom-control-label" for="1">ppn 11%</label>
                        </div>
                        <input readonly value="<?= set_value('pajak_bmd'); ?>" name="pajak_bmd" id="pajak" type="text" class="form-control d-none">
                        <?= form_error('pajakbmd', '<span class="text-danger small">', '</span>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 offset-md-2 text-md-right" for="sub_total">Sub Total</label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" value="<?= $subtotal ?>" name="total" id="total_" class="form-control d-none">
                            <input readonly value="<?= set_value('subtotal'); ?>" name="subtotal" id="sub_total" type="text" class="form-control" placeholder="Sub Total...">
                        </div>
                        <?= form_error('subtotal', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col offset-md-6">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script>
    function radioBtn(val) {
        $ttl = $('#total_').val();
        $subttl = $('#sub_total').val();
        // $hrg = $('#harga_masuk').val();
        if (val == 0) {
            $('#sub_total').val($ttl);
            $('#pajak').val(0);
        } else {
            $('#sub_total').val(parseInt($ttl) + parseInt($ttl * 11 / 100));
            $('#pajak').val($ttl * 11 / 100);
        }
    }
</script>
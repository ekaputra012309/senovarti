<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Add Barang Masuk
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('barangmasuk/add') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open('', [], ['id_barang_masuk' => $id_barang_masuk]); ?>
                <div class="modal-body">
                    <div class="row form-group">
                        <label class="col-md-4 text-md-right" for="barang_id">Barang</label>
                        <div class="col-md-6">
                            <input value="<?= set_value('barang_id'); ?>" name="barang_id" id="barang_id" type="text" class="form-control d-none">
                            <div class="input-group">
                                <input value="<?= set_value('nama_barang'); ?>" name="nama_barang" id="nama_barang" type="text" class="form-control" placeholder="Nama Barang..." readonly>
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <!-- <?= form_error('barang_id', '<small class="text-danger">', '</small>'); ?><br> -->
                            <?= form_error('nama_barang', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-md-4 text-md-right" for="jumlah_masuk">Jumlah Masuk</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input onkeyup="getJml()" value="<?= set_value('jumlah_masuk'); ?>" name="jumlah_masuk" id="jumlah_masuk" type="number" class="form-control" placeholder="Jumlah Masuk...">
                                <select name="satuan" id="satuan" class="custom-select">
                                    <option value="" selected disabled>Pilih Satuan</option>
                                    <?php foreach ($satuan as $s) : ?>
                                        <option <?= set_select('satuan', $s['id_satuan']) ?> value="<?= $s['id_satuan'] ?>"><?= $s['nama_satuan'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <?= form_error('jumlah_masuk', '<small class="text-danger">', '</small>'); ?><br>
                            <?= form_error('satuan', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="row form-group" id="ttstok">
                        <label class="col-md-4 text-md-right" for="total_stok">Ukuran P x L</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input id="panjang" type="number" class="form-control" step="0.1" placeholder="Panjang" required="">
                                <input id="lebar" type="number" class="form-control" step="0.1" placeholder="Lebar" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row form-group" id="ttstok">
                        <label class="col-md-4 text-md-right" for="">Weight</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input value="<?= set_value('weight'); ?>" name="weight" id="weight" type="number" class="form-control" step="0.1" placeholder="0" readonly>
                                <div class="input-group-append">
                                    <span class="btn btn-secondary">Kg</span>
                                </div>
                            </div>
                            <input type="text" name="ukuran" id="ukuran" class="form-control d-none">
                            <?= form_error('weight', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-md-4 text-md-right" for="total_stok">Jumlah dalam Pack</label>
                        <div class="col-md-6">
                            <input value="<?= set_value('qty_pack'); ?>" id="qty_pack" name="qty_pack" type="number" class="form-control" placeholder="Jumlah (Pack)">
                            <?= form_error('qty_pack', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-md-4 text-md-right" for="harga_masuk">Harga Satuan</label>
                        <div class="col-md-6">
                            <input value="<?= set_value('harga_masuk'); ?>" name="harga_masuk" id="harga_masuk" type="number" class="form-control" placeholder="Harga..." readonly>
                            <?= form_error('harga_masuk', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-md-4 text-md-right" for="total">Total</label>
                        <div class="col-md-6">
                            <input value="<?= set_value('total'); ?>" name="total" id="total_" type="number" class="form-control" placeholder="Total..." readonly>
                            <?= form_error('total', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary">Reset</button>
                    <button type="submit" class="btn btn-primary">Add Barang</button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Data Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped w-100" id="dataTable1">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th width="150">Nama Barang</th>
                            <th>Stok</th>
                            <th>Satuan</th>
                            <th>Weight</th>
                            <th>Harga</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        if ($barang) :
                            foreach ($barang as $b) :
                        ?>
                                <tr>
                                    <td><?= $b['id_brg']; ?></td>
                                    <td><?= $b['nama_barang']; ?></td>
                                    <td><?= $b['stok']; ?></td>
                                    <td><?= $b['nama_satuan']; ?></td>
                                    <td><?= $b['weight']; ?></td>
                                    <td><?= "Rp " . number_format($b['harga'], 0, ',', '.'); ?></td>
                                    <td>
                                        <button onclick="addtoTable('<?= $b['id_brg']; ?>')" type="button" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <tr>
                                <td colspan="7" class="text-center">
                                    Data Kosong
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        // Disable the submit button initially
        $('button[type="submit"]').prop('disabled', true);

        // Check the form fields on keyup event
        $('input[type="text"], input[type="number"], select').keyup(function() {
            var empty = false;

            // Check each form field for empty value
            $('input[type="text"], input[type="number"], select').each(function() {
                if ($(this).val() === '') {
                    empty = true;
                    return false; // Exit the loop if any field is empty
                }
            });

            // Enable or disable the submit button based on the empty flag
            if (empty) {
                $('button[type="submit"]').prop('disabled', true);
            } else {
                $('button[type="submit"]').prop('disabled', false);
            }
        });
    });


    function addtoTable(id) {
        let url = '<?= base_url('barangmasuk/getBrg/'); ?>' + id;
        // alert(url);
        $.getJSON(url, function(data) {
            $('#barang_id').val(data[0].id_brg);
            $('#nama_barang').val(data[0].nama_barang);
            $('#satuan').val(data[0].id_satuan);
            $('#weight').val(data[0].weight);
            $('#harga_masuk').val(data[0].harga);
            $('#jumlah_masuk').focus();
        });
        $('.modal').modal('hide');
    }

    function getJml() {
        $jml = $('#jumlah_masuk').val();
        $hrg = $('#harga_masuk').val();
        // alert($jml);
        if ($hrg == '') {
            alert('Mohon pilih data barang terlebih dahulu!');
        } else {
            $('#total_').val($jml * $hrg);
        }
    }

    // function radioBtn(val) {
    //     $jml = $('#jumlah_masuk').val();
    //     $hrg = $('#harga_masuk').val();
    //     if ($jml == '') {
    //         alert('Mohon isi jumlah masuk terlebih dahulu!');
    //     } else if (val == 0) {
    //         $('#total_').val($jml * $hrg);
    //     } else {
    //         $('#total_').val($jml * $hrg);
    //     }
    // }
</script>
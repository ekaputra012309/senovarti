<?php
foreach ($barangmasuk as $bm) :
?>
    <table width="100%" border="0">
        <tr>
            <th width="100" align="left">
                <img style="max-height: 120px; max-width: 120px;" src="<?= base_url('assets/img/avatar/' . $about['foto']) ?>" alt="logo">
            </th>
            <th>
                <h2 style="text-align:center; vertical-align:top"><?= $title ?></h2>
            </th>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <th colspan="3" align="left">ID Transaksi : <?= $bm['id_barang_masuk']; ?></th>
            <th colspan="2" align="right">Tanggal : <?= $bm['tanggal_masuk']; ?></th>
        </tr>
        <tr>
            <th style="border-bottom: 1px solid black;" colspan="5" align="left">Supplier : <?= $bm['nama_supplier']; ?></th>
        </tr>
        <tr>
            <td width="20">No. </td>
            <td>Nama Barang</td>
            <td>Qty</td>
            <td>Harga</td>
            <td width="100" align="center">Total</td>
        </tr>
        <?php
        $no = 1;
        $subtotal = 0;
        $this->db->join('barang b', 'bmd.barang_id = b.id_brg');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->where('id_barang_masuk', $bm['id_barang_masuk']);
        $q = $this->db->get('barang_masuk_d bmd')->result_array();
        foreach ($q as $v) {
            $total = $v['jumlah_masuk'] * $v['harga_masuk'];
        ?>
            <tr>
                <td><?= $no++; ?></td>
                <td><?= $v['nama_barang']; ?></td>
                <td><?= $v['jumlah_masuk'] . ' / ' . $v['nama_satuan']; ?></td>
                <td><?= number_format($v['harga_masuk'], 0, ',', '.'); ?></td>
                <td align="right"><?= number_format($total, 0, ',', '.'); ?></td>
            </tr>
        <?php
        }
        $gttl = $bm['total'] + $bm['pajak_bmd'];
        ?>
        <tr>
            <th id="doc" colspan="2"></th>
            <th id="doc" align="left">Total <br>PPN <br>Grand Total</th>
            <th id="doc" align="left">: IDR <br>: IDR <br>: IDR</th>
            <th id="doc" align="right">
                <?= number_format($bm['total'], 0, ',', '.'); ?> <br>
                <?= number_format($bm['pajak_bmd'], 0, ',', '.'); ?> <br>
                <?= number_format($gttl, 0, ',', '.'); ?>
            </th>
        </tr>
    </table>
<?php endforeach; ?>
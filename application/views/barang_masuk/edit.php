<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Edit Barang Masuk
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('barangmasuk') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open('', [], ['id_barang_masuk' => $barang_masuk['id_barang_masuk'], 'user_id' => $this->session->userdata('login_session')['user']]); ?>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="id_barang_masuk">ID Transaksi Barang Masuk</label>
                    <div class="col-md-4">
                        <input value="<?= set_value('id_barang_masuk', $barang_masuk['id_barang_masuk']); ?>" type="text" readonly="readonly" class="form-control">
                        <?= form_error('id_barang_masuk', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="tanggal_masuk">Tanggal Masuk</label>
                    <div class="col-md-4">
                        <input value="<?= set_value('tanggal_masuk', $barang_masuk['tanggal_masuk']); ?>" name="tanggal_masuk" id="tanggal_masuk" type="text" class="form-control date" placeholder="Tanggal Masuk...">
                        <?= form_error('tanggal_masuk', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="supplier_id">Supplier</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <select name="supplier_id" id="supplier_id" class="custom-select">
                                <option value="" selected disabled>Pilih Supplier</option>
                                <?php foreach ($supplier as $s) : ?>
                                    <option <?= $barang_masuk['supplier_id'] == $s['id_supplier'] ? 'selected' : ''; ?> <?= set_select('supplier_id', $s['id_supplier']) ?> value="<?= $s['id_supplier'] ?>"><?= $s['nama_supplier'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="input-group-append">
                                <a class="btn btn-primary" href="<?= base_url('supplier/add'); ?>"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        <?= form_error('supplier_id', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="barang_id">Barang</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <select name="barang_id" id="barang_id" class="custom-select">
                                <option value="" selected disabled>Pilih Barang</option>
                                <?php foreach ($barang as $b) : ?>
                                    <option <?= $barang_masuk['barang_id'] == $b['id_barang'] ? 'selected' : ''; ?> <?= set_select('barang_id', $b['id_barang']) ?> value="<?= $b['id_barang'] ?>"><?= $b['id_barang'] . ' | ' . $b['nama_barang'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="input-group-append">
                                <a class="btn btn-primary" href="<?= base_url('barang/add'); ?>"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        <?= form_error('barang_id', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group d-none" id="sstok">
                    <label class="col-md-4 text-md-right" for="stok">Stok</label>
                    <div class="col-md-5">
                        <input readonly="readonly" id="stok" type="number" class="form-control">
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="jumlah_masuk">Jumlah Masuk</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <input value="<?= set_value('jumlah_masuk', $barang_masuk['jumlah_masuk']); ?>" name="jumlah_masuk" id="jumlah_masuk" type="number" class="form-control" placeholder="Jumlah Masuk...">
                            <div class="input-group-append">
                                <select name="satuan_m" id="satuan_m" class="custom-select">
                                    <option value="" selected disabled>Satuan</option>
                                    <?php foreach ($satuan as $s) : ?>
                                        <option <?= $barang_masuk['satuan_m'] == $s['id_satuan'] ? 'selected' : ''; ?> <?= set_select('satuan_m', $s['id_satuan']) ?> value="<?= $s['id_satuan'] ?>"><?= $s['nama_satuan'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <!-- <span class="input-group-text" id="satuan">Satuan</span> -->
                            </div>
                        </div>
                        <?= form_error('jumlah_masuk', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group" id="ttstok">
                    <label class="col-md-4 text-md-right" for="total_stok">Ukuran P x L</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <input value="<?php 
                                $uk = explode(':',$barang_masuk['ukuran']);
                                echo set_value('panjang', $uk[0]); 
                            ?>" id="panjang" type="number" class="form-control" step="0.1" placeholder="P" required="">
                            <input value="<?= set_value('lebar', $uk[1])?>" id="lebar" type="number" class="form-control" step="0.1" placeholder="L" required="">
                            <input value="<?= set_value('lebar', $uk[2])?>" id="berat" type="number" class="form-control" step="0.1" placeholder="0 Kg" required="">                            
                        </div>
                        <input value="<?= set_value('ukuran', $barang_masuk['ukuran'])?>" type="text" name="ukuran" id="ukuran" class="form-control d-none">
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="qty_pcs">Jumlah dalam PCS</label>
                    <div class="col-md-5">
                        <input value="<?= set_value('qty_pcs', $barang_masuk['qty_pcs']); ?>" id="qty_pcs" name="qty_pcs" type="number" class="form-control" placeholder="Jumlah (PCS)" required="">
                    </div>
                </div>
                <div class="row form-group d-none" id="ttstok">
                    <label class="col-md-4 text-md-right" for="total_stok">Total Stok</label>
                    <div class="col-md-5">
                        <input readonly="readonly" id="total_stok" type="text" class="form-control">
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="harga_masuk">Harga Satuan</label>
                    <div class="col-md-5">
                        <input value="<?= set_value('harga_masuk', $barang_masuk['harga_masuk']); ?>" name="harga_masuk" id="harga_masuk" type="number" class="form-control" placeholder="Harga..">
                        <?= form_error('harga_masuk', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col offset-md-4">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Reset</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-4 col-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                            <a class="text-decoration-none" href="<?= base_url('invoice'); ?>"> Data Invoice </a>
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $po_invoice; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-file-invoice fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                            <a class="text-decoration-none" href="<?= base_url('purchaseorder'); ?>"> Data Purchase Order </a>
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $purchase_order; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-file fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                            <a class="text-decoration-none" href="<?= base_url('tandaterima'); ?>"> Data Tanda Terima </a>
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $tandaterima; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-receipt fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
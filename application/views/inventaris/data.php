<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Data Inventaris
                </h4>
            </div>
            <div class="col-auto">
                <a href="<?= base_url('inventaris/add') ?>" class="btn btn-sm btn-primary btn-icon-split">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">
                        Tambah Inventaris
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable1">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>Tanggal</th>
                    <th>File</th>
                    <th>Nama Item</th>
                    <th>Qty</th>
                    <th>Harga</th>
                    <th>Metode</th>
                    <th>Total</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($data_t) :
                    foreach ($data_t as $dt) :
                ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= longdate_indo($dt['tanggal']); ?></td>
                            <td>
                                <img style="max-height: 2cm; max-width: 5cm;" src="<?= base_url() ?>assets/img/inventaris/<?= $dt['fileup']; ?>" alt="" class="shadow-sm img-thumbnail">
                            </td>
                            <td><?= $dt['nama_item']; ?></td>
                            <td><?= $dt['qty']; ?></td>
                            <td><?= "Rp " . number_format($dt['harga'], 0, ',', '.'); ?></td>
                            <td><?= $dt['metode']; ?></td>
                            <td><?= "Rp " . number_format($dt['total'], 0, ',', '.'); ?></td>
                            <td>
                                <!-- <a href="<?= base_url('inventaris/edit/') . $dt['id_inventaris'] ?>" class="btn btn-warning btn-circle btn-sm"><i class="fa fa-edit"></i></a> -->
                                <a onclick="return confirm('Yakin ingin hapus?')" href="<?= base_url('inventaris/delete/') . $dt['id_inventaris'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-trash"></i></a>
                                <!-- <a href="<?= base_url('inventaris/cetak/') . $dt['id_inventaris'] ?>" class="btn btn-sm btn-info btn-circle"><i class="fa fa-print"></i></a> -->
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="9" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
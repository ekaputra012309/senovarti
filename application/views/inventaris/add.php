<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Tambah Inventaris
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('inventaris') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open_multipart(); ?>
                <div class="row form-group">
                    <label class="col-md-2 text-md-right" for="tanggal">Tanggal</label>
                    <div class="col-md-3">
                        <input value="<?= set_value('tanggal', date('Y-m-d')); ?>" name="tanggal" type="text" class="form-control date" placeholder="Tanggal...">
                        <?= form_error('tanggal', '<small class="text-danger">', '</small>'); ?>
                    </div>
                    <label class="col-md-2 text-md-right" for="vendor">Nama Vendor</label>
                    <div class="col-md-5">
                        <input value="<?= set_value('vendor'); ?>" name="vendor" id="vendor" type="text" class="form-control" placeholder="Nama Vendor">
                        <?= form_error('vendor', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-2 text-md-right" for="no_doc">No Dokumen</label>
                    <div class="col-md-3">
                        <input value="<?= set_value('no_doc'); ?>" name="no_doc" type="text" class="form-control" placeholder="No Dokumen">
                        <?= form_error('no_doc', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-2 text-md-right" for="nama_item">Nama Item</label>
                    <div class="col-md-3">
                        <input value="<?= set_value('nama_item'); ?>" name="nama_item" type="text" class="form-control" placeholder="Nama Item...">
                        <?= form_error('nama_item', '<small class="text-danger">', '</small>'); ?>
                    </div>
                    <label class="col-md-2 text-md-right" for="vendor">Spesifikasi</label>
                    <div class="col-md-5">
                        <textarea class="form-control" name="spesifikasi" id="spesifikasi" rows="3"><?= set_value('spesifikasi'); ?></textarea>
                        <?= form_error('spesifikasi', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-2 text-md-right" for="qty">Kuantititas</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <input value="<?= set_value('qty'); ?>" name="qty" id="qty" type="number" class="form-control" placeholder="Kuantitas">
                            <div class="input-group-append">
                                <select name="satuan" id="satuan" class="custom-select">
                                    <option value="pcs">PCS</option>
                                    <option value="set">SET</option>
                                </select>
                            </div>
                        </div>
                        <?= form_error('qty', '<small class="text-danger">', '</small>'); ?>
                    </div>
                    <label class="col-md-2 text-md-right" for="harga">Harga</label>
                    <div class="col-md-3">
                        <input value="<?= set_value('harga'); ?>" name="harga" id="harga" type="number" class="form-control" placeholder="Harga">
                        <?= form_error('harga', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-2 text-md-right" for="metode">Metode</label>
                    <div class="col-md-3">
                        <select name="metode" id="metode" class="custom-select">
                            <option value="0">Pilih--</option>
                            <option value="lunas">Lunas</option>
                            <option value="cicil">Cicil</option>
                        </select>
                        <?= form_error('metode', '<span class="text-danger small">', '</span>'); ?>
                    </div>
                </div>

                <!-- cicil -->
                <div class="ccl">
                    <div class="row form-group">
                        <label class="col-md-2 text-md-right" for="biaya_dp">DP</label>
                        <div class="col-md-3">
                            <input value="<?= set_value('biaya_dp', 0); ?>" name="biaya_dp" type="number" class="form-control" placeholder="DP">
                            <?= form_error('biaya_dp', '<small class="text-danger">', '</small>'); ?>
                        </div>
                        <label class="col-md-2 text-md-right" for="jangka_waktu">Jangka Waktu</label>
                        <div class="col-md-3">
                            <select name="jangka_waktu" id="jangka_waktu" class="custom-select">
                                <option value="0" selected>Pilih--</option>
                                <option value="6">6 Bulan</option>
                                <option value="12">12 Bulan</option>
                                <option value="24">24 Bulan</option>
                            </select>
                            <?= form_error('jangka_waktu', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-md-2 text-md-right" for="biaya_perbulan">Biaya per bulan</label>
                        <div class="col-md-3">
                            <input value="<?= set_value('biaya_perbulan', 0); ?>" name="biaya_perbulan" type="number" class="form-control" placeholder="Biaya / bulan">
                            <?= form_error('biaya_perbulan', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-md-2 text-md-right" for="fileup">Picture</label>
                        <div class="col-md-3">
                            <input type="file" name="fileup" id="fileup" value="<?= set_value('fileup'); ?>">
                        </div>
                        <label class="col-md-2 text-md-right" for="keterangan">Keterangan</label>
                        <div class="col-md-3">
                            <select name="keterangan" id="keterangan" class="custom-select">
                                <option value="">Pilih--</option>
                                <option value="sewa">Sewa</option>
                                <option value="aset">Aset</option>
                            </select>
                            <?= form_error('keterangan', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-2 text-md-right" for="total">Total</label>
                    <div class="col-md-3">
                        <input readonly value="<?= set_value('total', 0); ?>" name="total" id="total" type="text" class="form-control" placeholder="Total">
                        <?= form_error('total', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-9 offset-md-7">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Reset</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        // Disable the submit button initially
        // $('button[type="submit"]').prop('disabled', true);
        $('input[name="biaya_dp"]').prop('readonly', true);
        $('input[name="biaya_perbulan"]').prop('readonly', true);
        $('#jangka_waktu').prop('readonly', true);

        // // Check the form fields on keyup event
        // $('input[type="text"], input[type="number"], input[type="date"], select, textarea').keyup(function() {
        //     var empty = false;

        //     // Check each form field for empty value
        //     $('input[type="text"], input[type="number"], input[type="date"], select, textarea').each(function() {
        //         if ($(this).val() === '') {
        //             empty = true;
        //             return false; // Exit the loop if any field is empty
        //         }
        //     });

        //     // Enable or disable the submit button based on the empty flag
        //     if (empty) {
        //         $('button[type="submit"]').prop('disabled', true);
        //     } else {
        //         $('button[type="submit"]').prop('disabled', false);
        //     }
        // });

        $("#metode").change(function() {
            var selectedValue = $(this).val();
            if (selectedValue == 'lunas') {
                $('input[name="biaya_dp"]').val(0);
                $('input[name="biaya_perbulan"]').val(0);
                $('input[name="biaya_dp"]').prop('readonly', true);
                $('input[name="biaya_perbulan"]').prop('readonly', true);
                $('#jangka_waktu').prop('readonly', true);
            } else {
                $('input[name="biaya_dp"]').val('');
                $('input[name="biaya_perbulan"]').val('');
                $('input[name="biaya_dp"]').prop('readonly', false);
                $('input[name="biaya_perbulan"]').prop('readonly', false);
                $('#jangka_waktu').prop('readonly', false);
                $('input[name="biaya_dp"]').focus();
            }
        });

        $('#harga').on('keyup', function() {
            if ($('#qty').val() === '') {
                alert('Kuantitas tidak boleh kosong!');
                $('#qty').focus();
            } else {
                calculateTotal();
            }
        });

        function calculateTotal() {
            var quantity = parseInt($('#qty').val());
            var price = parseInt($('#harga').val());

            if (!isNaN(quantity) && !isNaN(price)) {
                var total = quantity * price;
                $('#total').val(total);
            }
        }
    });
</script>
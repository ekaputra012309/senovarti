<!DOCTYPE html>
<html>

<head>
    <title><?= $title . ' | ' . $about['nama_perusahaan']; ?></title>
    <style>
        table#doc,
        th,
        td {
            padding: 5px;
        }

        /* hide / hapus header & footer window.print()*/
        @media print {
            @page {
                <?= $page; ?>margin: 1.3cm 1.3cm 1.3cm 1.3cm !important
            }
        }

        table {
            border-collapse: collapse;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        th#doc {
            border-top: 1px solid black;
            border-bottom: 1px solid black;
        }
    </style>
</head>

<body>

    <?= $contents; ?>
    <script>
        window.print();
    </script>

</body>

</html>
<?php
foreach ($barangkeluar as $bk) :
?>
    <table width="100%" border="0">
        <tr>
            <th width="100" align="left">
                <img style="max-height: 120px; max-width: 120px;" src="<?= base_url('assets/img/avatar/' . $about['foto']) ?>" alt="logo">
            </th>
            <th>
                <h2 style="text-align:center; vertical-align:top"><?= $title ?></h2>
            </th>
        </tr>
    </table>
    <table width="100%" id="doc">
        <tr>
            <th id="doc" colspan="3" align="left">ID Transaksi : <?= $bk['id_barang_keluar']; ?></th>
            <th id="doc" colspan="3" align="right">Tanggal : <?= $bk['tanggal_keluar']; ?></th>
        </tr>
        <tr>
            <td width="20">No. </td>
            <td>Nama Barang</td>
            <td>Sisa Stok</td>
            <td>Qty</td>
            <td>Harga</td>
            <td width="100" align="center">Total</td>
        </tr>
        <?php
        $no = 1;
        $subtotal = 0;
        $this->db->join('barang b', 'bkd.barang_id = b.id_brg');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->where('id_barang_keluar', $bk['id_barang_keluar']);
        $q = $this->db->get('barang_keluar_d bkd')->result_array();
        foreach ($q as $v) {
            $total = $v['jumlah_keluar'] * $v['harga_keluar'];
            $sat = $this->admin->get('satuan', ['id_satuan' => $v['satuan_k']])['nama_satuan'];
        ?>
            <tr>
                <td><?= $no++; ?></td>
                <td><?= $v['nama_barang']; ?></td>
                <td><?= $v['stok'] . ' ' . $v['nama_satuan']; ?></td>
                <td><?= $v['jumlah_keluar'] . ' ' . $sat; ?></td>
                <td><?= number_format($v['harga_keluar'], 0, ',', '.'); ?></td>
                <td align="right"><?= number_format($total, 0, ',', '.'); ?></td>
            </tr>
        <?php
        }
        $gttl = $bk['total'] + $bk['pajak_bkd'];
        ?>
        <tr>
            <th id="doc" colspan="3"></th>
            <th id="doc" align="left">Total <br>PPN <br>Grand Total</th>
            <th id="doc" align="left">: IDR <br>: IDR <br>: IDR</th>
            <th id="doc" align="right">
                <?= number_format($bk['total'], 0, ',', '.'); ?> <br>
                <?= number_format($bk['pajak_bkd'], 0, ',', '.'); ?> <br>
                <?= number_format($gttl, 0, ',', '.'); ?>
            </th>
        </tr>
    </table>
<?php endforeach; ?>
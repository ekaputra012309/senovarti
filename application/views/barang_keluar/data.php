<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Riwayat Data Barang Keluar
                </h4>
            </div>
            <div class="col-auto">
                <a href="<?= base_url('barangkeluar/add') ?>" class="btn btn-sm btn-primary btn-icon-split">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">
                        Input Barang keluar
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>No Transaksi</th>
                    <th>Tanggal keluar</th>
                    <th>Sub Total</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($barangkeluar) :
                    foreach ($barangkeluar as $bk) :
                ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $bk['id_barang_keluar']; ?></td>
                            <td><?= $bk['tanggal_keluar']; ?></td>
                            <td>Rp <?= number_format($bk['total'] + $bk['pajak_bkd'], 0, ',', '.'); ?></td>
                            <td>
                                <?php if (is_admin() || is_owner()) : ?>
                                    <a onclick="return confirm('Yakin ingin hapus?')" href="<?= base_url('barangkeluar/delete/') . $bk['id_barang_keluar'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-trash"></i></a>
                                <?php endif ?>
                                <button type="button" class="btn btn-success btn-circle btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg-<?= $bk['id_barang_keluar']; ?>">
                                    <i class="fa fa-eye"></i>
                                </button>
                                <!-- modal info barang keluar -->
                                <div class="modal fade bd-example-modal-lg-<?= $bk['id_barang_keluar']; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="ModalinfoBWLabel">Detail Transaksi</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table">
                                                    <tr>
                                                        <th colspan="3">ID Transaksi : <?= $bk['id_barang_keluar']; ?></th>
                                                        <th colspan="3" class="text-right">Tanggal : <?= $bk['tanggal_keluar']; ?></th>
                                                    </tr>
                                                </table>
                                                <table class="table">
                                                    <tr>
                                                        <td width="20">No. </td>
                                                        <td>Nama Barang</td>
                                                        <td>Sisa Stok</td>
                                                        <td>Qty</td>
                                                        <td>Harga</td>
                                                        <td class="text-center">Total</td>
                                                    </tr>
                                                    <?php
                                                    $no = 1;
                                                    $subtotal = 0;
                                                    $this->db->join('barang b', 'bkd.barang_id = b.id_brg');
                                                    $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
                                                    $this->db->where('id_barang_keluar', $bk['id_barang_keluar']);
                                                    $q = $this->db->get('barang_keluar_d bkd')->result_array();
                                                    foreach ($q as $v) {
                                                        $total = $v['jumlah_keluar'] * $v['harga_keluar'];
                                                        $sat = $this->admin->get('satuan', ['id_satuan' => $v['satuan_k']])['nama_satuan'];
                                                    ?>
                                                        <tr>
                                                            <td><?= $no++; ?></td>
                                                            <td><?= $v['nama_barang']; ?></td>
                                                            <td><?= $v['stok'] . ' ' . $v['nama_satuan']; ?></td>
                                                            <td><?= $v['jumlah_keluar'] . ' ' . $sat; ?></td>
                                                            <td><?= number_format($v['harga_keluar'], 0, ',', '.'); ?></td>
                                                            <td class="text-right"><?= number_format($total, 0, ',', '.'); ?></td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    $gttl = $bk['total'] + $bk['pajak_bkd'];
                                                    ?>
                                                    <tr>
                                                        <th colspan="3"></th>
                                                        <th>Total</th>
                                                        <th class="">: &nbsp; &nbsp; IDR</th>
                                                        <th class="text-right"><?= number_format($bk['total'], 0, ',', '.'); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3"></th>
                                                        <th>PPN</th>
                                                        <th class="">: &nbsp; &nbsp; IDR</th>
                                                        <th class="text-right"><?= number_format($bk['pajak_bkd'], 0, ',', '.'); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3"></th>
                                                        <th>Grand Total</th>
                                                        <th class="">: &nbsp; &nbsp; IDR</th>
                                                        <th class="text-right"><?= number_format($gttl, 0, ',', '.'); ?></th>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <a target="_blank" href="<?= base_url('barangkeluar/cetak/') . $bk['id_barang_keluar'] ?>" class="btn btn-danger"><i class="fa fa-print"></i> Cetak</a>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="8" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Input Barang keluar
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('barangkeluar') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open('barangkeluar/add_p', [], ['id_barang_keluar' => $id_barang_keluar]); ?>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="id_barang_keluar">ID Transaksi Barang keluar</label>
                    <div class="col-md-4">
                        <input value="<?= $id_barang_keluar; ?>" type="text" readonly="readonly" class="form-control">
                        <?= form_error('id_barang_keluar', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="tanggal_keluar">Tanggal keluar</label>
                    <div class="col-md-4">
                        <input value="<?= set_value('tanggal_keluar', date('Y-m-d')); ?>" name="tanggal_keluar" id="tanggal_keluar" type="text" class="form-control date" placeholder="Tanggal keluar...">
                        <?= form_error('tanggal_keluar', '<small class="text-danger">', '</small>'); ?>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-primary" href="<?= base_url('barangkeluar/add_d') ?>">
                            <i class="fas fa-plus fa-sm fa-fw mr-2 text-gray-400"></i>
                            Add Barang
                        </a>
                    </div>
                </div>
                <div class="row form-group">
                    <table class="table table-striped w-100 dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th>No. </th>
                                <th>Nama Barang</th>
                                <th>Sisa Stok</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $subtotal = 0;
                            if ($barangkeluar_d) :
                                foreach ($barangkeluar_d as $bk) :
                                    $total = $bk['jumlah_keluar'] * $bk['harga_keluar'];
                                    $sat = $this->admin->get('satuan', ['id_satuan' => $bk['satuan_k']])['nama_satuan'];

                            ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $bk['nama_barang']; ?></td>
                                        <td><?= $bk['stok'] . ' ' . $bk['nama_satuan']; ?></td>
                                        <td><?= $bk['jumlah_keluar'] . ' ' . $sat; ?></td>
                                        <td><?= $bk['harga_keluar']; ?></td>
                                        <td><?= $total; ?></td>
                                        <td>
                                            <a href="<?= base_url('barangkeluar/delete_d/') . $bk['id_bkd'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php $subtotal += $total;
                                endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td colspan="7" class="text-center">
                                        Data Kosong
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="row form-group">
                    <label class="col-md-6 text-md-right" for="pajak_bkd">Pajak</label>
                    <div class="col-md-6">
                        <div class="custom-control custom-radio">
                            <input onchange="radioBtn('0')" <?= set_radio('pajakbkd', '0'); ?> value="0" type="radio" id="0" name="pajakbkd" class="custom-control-input">
                            <label class="custom-control-label" for="0">Tidak Ada Pajak</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input onchange="radioBtn('1')" <?= set_radio('pajakbkd', '1'); ?> value="1" type="radio" id="1" name="pajakbkd" class="custom-control-input">
                            <label class="custom-control-label" for="1">ppn 15%</label>
                        </div>
                        <input readonly value="<?= set_value('pajak_bkd'); ?>" name="pajak_bkd" id="pajak" type="text" class="form-control d-none">
                        <?= form_error('pajak_bkd', '<span class="text-danger small">', '</span>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 offset-md-2 text-md-right" for="sub_total">Sub Total</label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" value="<?= $subtotal ?>" name="total" id="total_" class="form-control d-none">
                            <input readonly value="<?= set_value('sub_total', $subtotal); ?>" name="subtotal" id="sub_total" type="text" class="form-control" placeholder="Sub Total...">
                        </div>
                        <?= form_error('subtotal', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col offset-md-6">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script>
    function radioBtn(val) {
        $ttl = $('#total_').val();
        $subttl = $('#sub_total').val();
        // $hrg = $('#harga_keluar').val();
        if (val == 0) {
            $('#sub_total').val($ttl);
            $('#pajak').val(0);
        } else {
            $('#sub_total').val(parseInt($ttl) + parseInt($ttl * 15 / 100));
            $('#pajak').val($ttl * 15 / 100);
        }
    }
</script>
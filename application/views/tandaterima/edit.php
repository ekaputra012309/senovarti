<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Edit Tanda Terima
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('tandaterima') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open_multipart('', [], ['id_tandaterima' => $td['id_tandaterima']]); ?>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="dari">Dari</label>
                    <div class="col-md-9">
                        <input value="<?= set_value('dari', $td['dari']); ?>" name="dari" id="dari" type="text" class="form-control" placeholder="Dari...">
                        <?= form_error('dari', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="kepada">Kepada</label>
                    <div class="col-md-9">
                        <input value="<?= set_value('kepada', $td['kepada']); ?>" name="kepada" id="kepada" type="text" class="form-control" placeholder="Kepada...">
                        <?= form_error('kepada', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="tanggal">Tanggal</label>
                    <div class="col-md-9">
                        <input value="<?= set_value('tanggal', $td['tanggal']); ?>" name="tanggal" type="text" class="form-control date" placeholder="Tanggal...">
                        <?= form_error('tanggal', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="nominal">Nominal</label>
                    <div class="col-md-9">
                        <input value="<?= set_value('nominal', $td['nominal']); ?>" name="nominal" id="nominal" type="number" class="form-control" placeholder="Nominal...">
                        <?= form_error('nominal', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="keterangan">Keterangan</label>
                    <div class="col-md-9">
                        <textarea name="keterangan" id="keterangan" class="form-control" rows="2" placeholder="Keterangan..."><?= set_value('keterangan', $td['keterangan']); ?></textarea>
                        <?= form_error('keterangan', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="fileup">Upload file</label>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-9">
                                <input type="file" name="fileup" id="fileup" value="<?= set_value('fileup', $td['fileup']); ?>">
                                <!-- <?= form_error('fileup', '<small class="text-danger">', '</small>'); ?> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-9 offset-md-3">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        // Disable the submit button initially
        $('button[type="submit"]').prop('disabled', true);

        // Check the form fields on keyup event
        $('input[type="text"], input[type="number"],input[type="date"], textarea').keyup(function() {
            var empty = false;

            // Check each form field for empty value
            $('input[type="text"], input[type="number"],input[type="date"], textarea').each(function() {
                if ($(this).val() === '') {
                    empty = true;
                    return false; // Exit the loop if any field is empty
                }
            });

            // Enable or disable the submit button based on the empty flag
            if (empty) {
                $('button[type="submit"]').prop('disabled', true);
            } else {
                $('button[type="submit"]').prop('disabled', false);
            }
        });
    });
</script>
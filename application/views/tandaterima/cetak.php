<?php
foreach ($data_t as $b) :
?>
    <table width="100%" border="0">
        <tr>
            <th width="100" align="left"><img style="max-height: 120px; max-width: 120px;" src="<?= base_url('assets/img/avatar/' . $about['foto']) ?>" alt="logo"></th>
            <th>
                <h2 style="text-align:center">TANDA TERIMA</h2>
            </th>
        </tr>
    </table>
    <hr>
    <table width="100%" id="doc">
        <tr>
            <th style="text-align: left;" width="10%">Dari</th>
            <th width="20">: </th>
            <th style="text-align: left;"><?= $b['dari'] ?></th>
        </tr>
        <tr>
            <th style="text-align: left;" width="10%">Kepada</th>
            <th width="20">: </th>
            <th style="text-align: left;"><?= $b['kepada'] ?></th>
        </tr>
        <tr>
            <th style="text-align: left;" width="10%">Hari/Tanggal</th>
            <th width="20">: </th>
            <th style="text-align: left;"><?= longdate_indo($b['tanggal']) ?></th>
        </tr>
        <tr>
            <th style="text-align: left;" width="10%">Nominal</th>
            <th width="20">: </th>
            <th style="text-align: left;"><?= number_format($b['nominal'], 0, ',', '.') ?></th>
        </tr>
        <tr>
            <th style="text-align: left;" width="10%">Terbilang</th>
            <th width="20">: </th>
            <th style="text-align: left;"><?= ucfirst(terbilang($b['nominal'])) ?></th>
        </tr>
        <tr>
            <th style="text-align: left;" width="10%">Keterangan</th>
            <th width="20">: </th>
            <th style="text-align: left;"><?= $b['keterangan'] ?></th>
        </tr>
    </table>
    <hr>
    <br>

    <table width="100%">
        <tr>
            <th style="text-align: center;">Bukti Pembayaran</th>
        </tr>
        <tr>
            <td style="text-align: center;">
                <?php
                $imageUrl = 'assets/img/tandaterima/' . $b['fileup'];
                $imageSize = getimagesize($imageUrl);
                if ($imageSize !== false) {
                    $width = $imageSize[0];
                    $height = $imageSize[1];
                    if ($width > $height) { ?>
                        <img style="max-width: 500px;" src="<?= base_url($imageUrl) ?>" alt="logo">;
                    <?php } else { ?>
                        <img style="max-height: 400px; " src="<?= base_url($imageUrl) ?>" alt="logo">;
                <?php }
                }
                ?>
            </td>
        </tr>
    </table>

<?php endforeach; ?>
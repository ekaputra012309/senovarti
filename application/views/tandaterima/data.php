<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Data Tanda Terima
                </h4>
            </div>
            <div class="col-auto">
                <a href="<?= base_url('tandaterima/add') ?>" class="btn btn-sm btn-primary btn-icon-split">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">
                        Tambah Tanda Terima Barang
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped" id="dataTable1">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>File</th>
                    <th>Nama Tanda Terima</th>
                    <th>Tanggal</th>
                    <th>Nominal</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($data_t) :
                    foreach ($data_t as $dt) :
                ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td>
                                <img style="max-height: 2cm; max-width: 5cm;" src="<?= base_url() ?>assets/img/tandaterima/<?= $dt['fileup']; ?>" alt="" class="shadow-sm img-thumbnail">
                            </td>
                            <td><?= $dt['kepada']; ?></td>
                            <td><?= longdate_indo($dt['tanggal']); ?></td>
                            <td><?= "Rp " . number_format($dt['nominal'], 0, ',', '.'); ?></td>
                            <td>
                                <a href="<?= base_url('tandaterima/edit/') . $dt['id_tandaterima'] ?>" class="btn btn-warning btn-circle btn-sm"><i class="fa fa-edit"></i></a>
                                <a onclick="return confirm('Yakin ingin hapus?')" href="<?= base_url('tandaterima/delete/') . $dt['id_tandaterima'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-trash"></i></a>
                                <a href="<?= base_url('tandaterima/cetak/') . $dt['id_tandaterima'] ?>" target="_blank" class="btn btn-sm btn-info btn-circle"><i class="fa fa-file-pdf"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="5" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
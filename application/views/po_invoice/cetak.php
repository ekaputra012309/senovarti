<?php
foreach ($datainv as $iv) :
    $noinv = explode('-', $iv['no_inv']);
    if ($iv['status'] == 0) {
        $st = 'On Process';
    } else {
        $st = 'Confirmed';
    }
?>
    <table width="100%" border="0">
        <tr>
            <th rowspan="5" align="left"><img style="max-height: 120px; max-width: 120px;" src="<?= base_url('assets/img/avatar/' . $about['foto']) ?>" alt="logo"></th>
            <th colspan="2">
                <h2 align="right">INVOICE</h2>
            </th>
        </tr>
        <tr>
            <th width="110" align="left">NO INVOICE</th>
            <th width="110" align="right"><?= $noinv[0] . '/' . $noinv[1] . '/' . $noinv[2] ?></th>
        </tr>
        <tr>
            <th width="110" align="left">Tanggal</th>
            <th width="110" align="right"><?= date_format(new DateTime($iv['tgl_']), 'd/m/Y'); ?></th>

        </tr>
        <tr>
            <th width="110" align="left">Status</th>
            <th width="110" align="right"><?= $st ?></th>
        </tr>
        <tr>
            <th width="110" align="left">Tanggal Lunas</th>
            <th width="110" align="right"><?= $iv['tgl_lns'] ?></th>
        </tr>
    </table>
    <!-- info vendor -->
    <hr>
    <table width="100%">
        <tr>
            <td style="border-bottom: 1.5px solid;" width="45%" align="left">Info Perusahaan</td>
            <td width="10%" align="center"></td>
            <td style="border-bottom: 1.5px solid;" width="45%" align="left">Info Supplier</td>
        </tr>
        <tr>
            <th width="45%" align="left"><?= $about['nama_perusahaan'] ?></th>
            <th width="10%" align="center"></th>
            <th width="45%" align="left"><?= $iv['nama_supplier'] ?></th>
        </tr>
        <tr>
            <td width="45%" align="left"><?= $about['deskripsi'] ?></td>
            <td width="10%" align="center"></td>
            <td width="45%" align="left">
                <?= $iv['alamat'] ?> <br>
                <?= $iv['no_telp'] ?> <br>
                <?= $iv['nm_bank'] ?> <br>
                <?= $iv['no_rek'] ?> <br>
            </td>
        </tr>
    </table>
    <br>
    <!-- info po detail -->
    <table width="100%" border="0" style="border-top: 1px solid; border-bottom:1px solid">
        <tr>
            <th>No</th>
            <th width="50%">Produk</th>
            <th width="5%">Kuantitas</th>
            <th width="15%">Harga</th>
            <th width="5%">Diskon</th>
            <th width="5%">Pajak</th>
            <th width="20%">Total</th>
        </tr>
        <?php
        $no = 1;
        $subtotal = 0;
        foreach ($datainv_d as $ind) :
            // $total = $ind['qty'] * $ind['harga_d'];
        ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= '<b>' . $ind['id_barang'] . '</b> - ' . $ind['nama_barang']; ?></td>
                <td align="center"><?= $ind['qty'] . ' ' . $ind['nama_satuan']; ?></td>
                <td align="right"><?= number_format($ind['harga_d'], 0, ',', '.'); ?></td>
                <td align="center"><?= $ind['diskon'] . ' %'; ?></td>
                <td align="center"><?= $ind['pajak_d']; ?></td>
                <td align="right"><?= number_format($ind['total'], 0, ',', '.'); ?></td>
            </tr>
        <?php $subtotal += $ind['total'];
        endforeach; ?>
    </table>
    <br>
    <!-- footer table -->
    <table width="100%" border="0">
        <tr>
            <td width="75%" colspan="4" align="right">Sub Total</td>
            <td width="6%" align="right">: IDR</td>
            <td width="19%" align="right"><?= number_format($subtotal, 0, ',', '.') ?></td>
        </tr>
        <tr>
            <td width="75%" colspan="4" align="right">Diskon</td>
            <td width="6%" align="right">: IDR</td>
            <td width="19%" align="right"><?= number_format($iv['sum_diskon'], 0, ',', '.') ?></td>
        </tr>
        <tr>
            <td width="75%" colspan="4" align="right">Pajak</td>
            <td width="6%" align="right">: IDR</td>
            <td width="19%" align="right"><?= number_format($iv['sum_pajak'], 0, ',', '.') ?></td>
        </tr>
        <tr>
            <th width="58%" colspan="3"></th>
            <th width="17%" style="border-top: 1px solid;" align="right">Jumlah Tertagih</th>
            <th style="border-top: 1px solid;" width="6%" align="right">: IDR</th>
            <th style="border-top: 1px solid;" width="19%" align="right"><?= number_format($iv['jumlah_tertagih'], 0, ',', '.') ?></th>
        </tr>
    </table>
    <br><br>
    <!-- footer -->
    <table width="100%" border="0">
        <tr>
            <td width="75%" colspan="4" align="left">Catatan : </td>
            <td width="25%" align="center"><?= longdate_indo($iv['tgl_']); ?></td>
        </tr>
        <tr>
            <td width="75%" rowspan="4" colspan="4" style="vertical-align: top; text-align:left;"><?= $iv['catatan'] ?> </td>
            <td width="25%" align="center">Mengetahui</td>
        </tr>
        <tr>
            <td width="25%" align="center"><br></td>
        </tr>
        <tr>
            <td width="25%" align="center"><br></td>
        </tr>
        <tr>
            <td width="25%" align="center">Nama Admin</td>
        </tr>

    </table>
<?php endforeach; ?>
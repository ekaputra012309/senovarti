<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Input Invoice
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('invoice') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <!-- isi dari top invoice -->
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open_multipart('invoice/add_p', []); ?>
                <div class="row form-group">
                    <label class="col-md-2 text-md-right" for="no_inv">No PO</label>
                    <div class="col-md-4">
                        <input readonly value="<?= set_value('no_inv', $no_inv); ?>" name="no_inv" id="no_inv" type="text" class="form-control" placeholder="No invoice">
                        <?= form_error('no_inv', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-2 text-md-right" for="tgl_">Tanggal Kirim Barang</label>
                    <div class="col-md-4">
                        <input value="<?= set_value('tgl_', date('Y-m-d')); ?>" name="tgl_" id="tgl_" type="text" class="form-control date" placeholder="Tanggal Kirim Barang">
                        <?= form_error('tgl_', '<small class="text-danger">', '</small>'); ?>
                    </div>
                    <label class="col-md-2 text-md-right" for="tgl_tempo">Tanggal Jatuh Tempo</label>
                    <div class="col-md-3">
                        <select name="tgl_tempo" id="tgl_tempo" class="custom-select">
                            <option value="" selected disabled>Pilih Tanggal</option>
                            <option value="7">7 Hari</option>
                            <option value="14">14 Hari</option>
                            <option value="30">30 Hari</option>
                        </select>
                        <!-- <input value="<?= set_value('tgl_tempo'); ?>" name="tgl_tempo" id="tgl_tempo" type="text" class="form-control date1" placeholder="Tanggal Jatuh Tempo"> -->
                        <?= form_error('tgl_tempo', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-2 text-md-right" for="tgl_faktur">Tanggal Tuker Faktur</label>
                    <div class="col-md-4">
                        <input value="<?= set_value('tgl_faktur'); ?>" name="tgl_faktur" id="tgl_faktur" type="text" class="form-control date1" placeholder="Tanggal Tuker Faktur">
                        <?= form_error('tgl_faktur', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-2 text-md-right" for="supplier_id">Supplier</label>
                    <div class="col-md-4">
                        <div class="input-group">
                            <select name="supplier_id" id="supplier_id" class="custom-select">
                                <option value="" selected disabled>Pilih Supplier</option>
                                <?php foreach ($supplier as $s) : ?>
                                    <option <?= set_select('supplier_id', $s['id_supplier']) ?> value="<?= $s['id_supplier'] ?>"><?= $s['nama_supplier'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <!-- <div class="input-group-append">
                                <a class="btn btn-primary" href="<?= base_url('invoice/add_sup'); ?>"><i class="fa fa-plus"></i></a>
                            </div> -->
                        </div>
                        <?= form_error('supplier_id', '<small class="text-danger">', '</small>'); ?>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-primary" href="<?= base_url('invoice/add_d') ?>">
                            <i class="fas fa-plus fa-sm fa-fw mr-2 text-gray-400"></i>
                            Add Produk
                        </a>
                    </div>
                </div>
                <!-- table data produk -->
                <div class="row form-group">
                    <table class="table table-striped w-100 dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Diskon</th>
                                <th>Pajak</th>
                                <th>Total</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $subtotal = 0;
                            if ($invoice_d) :
                                foreach ($invoice_d as $pod) :
                                    // $total = $pod['qty'] * $pod['harga_d'];
                            ?>
                                    <tr>
                                        <td><?= $pod['nama_barang']; ?></td>
                                        <td><?= $pod['qty'] . ' ' . $pod['nama_satuan']; ?></td>
                                        <td><?= $pod['harga_d']; ?></td>
                                        <td><?= $pod['diskon'] . ' %'; ?></td>
                                        <td><?= $pod['pajak_d']; ?></td>
                                        <td><?= $pod['total']; ?></td>
                                        <td>
                                            <a href="<?= base_url('invoice/del_d/') . $pod['idinv'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php $subtotal += $pod['total'];
                                endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td colspan="7" class="text-center">
                                        Data Kosong
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- Left column with 1 row -->
                            <div class="row form-group">
                                <label for="catatan" class="col-sm-3 col-form-label text-sm-right">Catatan</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="catatan" id="catatan" rows="4"></textarea>
                                </div>
                                <?= form_error('catatan', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <!-- Right column with more than 2 rows -->
                            <div class="row form-group">
                                <label for="subtotal" class="col-sm-4 col-form-label text-sm-right">Sub Total</label>
                                <div class="col-sm-8">
                                    <input value="<?= set_value('subtotal', $subtotal); ?>" name="subtotal" id="subtotal" type="text" class="form-control" placeholder="Sub Total ..." readonly="readonly">
                                </div>
                                <?= form_error('subtotal', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="row form-group d-none">
                                <label for="sum_pajak" class="col-sm-4 col-form-label text-sm-right">Pajak</label>
                                <div class="col-sm-8">
                                    <input value="<?= set_value('sum_pajak', '0'); ?>" name="sum_pajak" id="sum_pajak" type="text" class="form-control" placeholder="Pajak ..." readonly="readonly">
                                </div>
                                <?= form_error('sum_pajak', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="row form-group d-none">
                                <label for="sum_diskon" class="col-sm-4 col-form-label text-sm-right">Diskon</label>
                                <div class="col-sm-8">
                                    <input value="<?= set_value('sum_diskon', '0'); ?>" name="sum_diskon" id="sum_diskon" type="text" class="form-control" placeholder="Diskon ..." readonly="readonly">
                                </div>
                                <?= form_error('sum_diskon', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="row form-group">
                                <label for="jumlah_tertagih" class="col-sm-4 col-form-label text-sm-right">Jumlah Tertagih</label>
                                <div class="col-sm-8">
                                    <input value="<?= set_value('jumlah_tertagih', $subtotal); ?>" name="jumlah_tertagih" id="jumlah_tertagih" type="text" class="form-control" placeholder="Jumlah Tertagih ..." readonly="readonly">
                                </div>
                                <?= form_error('jumlah_tertagih', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="row form-group">
                                <label for="fileup" class="col-sm-4 col-form-label text-sm-right">Upload file</label>
                                <div class="col-sm-8">
                                    <input type="file" name="fileup" id="fileup" value="<?= set_value('fileup'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col offset-md-8">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Reset</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        // Disable the submit button initially
        $('button[type="submit"]').prop('disabled', true);

        // Check the form fields on keyup event
        $('input[type="text"], input[type="number"], input[type="date"], textarea, select').keyup(function() {
            var empty = false;

            // Check each form field for empty value
            $('input[type="text"], input[type="number"], input[type="date"], textarea, select').each(function() {
                if ($(this).val() === '') {
                    empty = true;
                    return false; // Exit the loop if any field is empty
                }
            });

            // Enable or disable the submit button based on the empty flag
            if (empty) {
                $('button[type="submit"]').prop('disabled', true);
            } else {
                $('button[type="submit"]').prop('disabled', false);
            }
        });
    });
</script>
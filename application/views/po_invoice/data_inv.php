<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Riwayat Data Invoice
                </h4>
            </div>
            <div class="col-auto">
                <a href="<?= base_url('invoice/add') ?>" class="btn btn-sm btn-primary btn-icon-split">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">
                        Input Invoice
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>No Invoice</th>
                    <th>Tanggal</th>
                    <th>Tanggal Tempo</th>
                    <th>Jumlah Tagihan</th>
                    <th>Tanggal Lunas</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($datainvoice) :
                    foreach ($datainvoice as $in) :
                        $noinv = explode('-', $in['no_inv']);
                ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $noinv[0] . '/' . $noinv[1] . '/' . $noinv[2] ?></td>
                            <td><?= $in['tgl_']; ?></td>
                            <td><?= $in['tgl_tempo']; ?></td>
                            <td>Rp <?= number_format($in['jumlah_tertagih'], 2, ',', '.') ?></td>
                            <td><?= $in['tgl_lns']; ?></td>
                            <td>
                                <a href="<?= base_url('invoice/toggle/') . $in['no_inv'] ?>" class="btn btn-sm <?= $in['status'] ? 'btn-success' : 'btn-secondary' ?>" title="<?= $in['status'] ? 'Ubah Status ke Piutang' : 'Ubah Status ke Lunas' ?>"><?= $in['status'] ? 'Lunas' : 'Piutang' ?></i></a>
                            </td>
                            <td>
                                <a href="<?= base_url('invoice/cetak/') . $in['no_inv'] ?>" target="_blank" class="btn btn-info btn-sm btn-circle"><i class="fa fa-print"></i></a>
                                <a onclick="return confirm('Yakin ingin menghapus data?')" href="<?= base_url('invoice/delete/') . $in['no_inv'] ?>" class="btn btn-circle btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="8" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
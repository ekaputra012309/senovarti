<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Form About
                </h4>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open_multipart('', [], ['id' => $about['id']]); ?>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="foto">Logo</label>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-3">
                                <img src="<?= base_url() ?>assets/img/avatar/<?= $about['foto']; ?>" alt="" class="shadow-sm img-thumbnail">
                            </div>
                            <div class="col-9">
                                <input type="file" name="foto" id="foto">
                                <?= form_error('foto', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="nama_perusahaan">Nama Perusahaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-building"></i></span>
                            </div>
                            <input value="<?= set_value('nama_perusahaan', $about['nama_perusahaan']); ?>" name="nama_perusahaan" id="nama_perusahaan" type="text" class="form-control" placeholder="Nama Perusahaan Anda...">
                        </div>
                        <?= form_error('nama_perusahaan', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="deskripsi">Deskripsi</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-book"></i></span>
                            </div>
                            <textarea class="form-control" placeholder="Deskripsi" name="deskripsi" id="deskripsi" cols="30" rows="3"><?= set_value('deskripsi', $about['deskripsi']); ?></textarea>
                        </div>
                        <?= form_error('deskripsi', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <hr>
                <div class="row form-group">
                    <div class="col-md-9 offset-md-3">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <!-- <button type="reset" class="btn btn-secondary">Reset</button> -->
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Edit Jenis
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('jenis') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open('', [], ['id_jenis' => $jenis['id_jenis']]); ?>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="nama_jenis">Nama Kategori</label>
                    <div class="col-md-9">
                        <input value="<?= set_value('nama_jenis', $jenis['nama_jenis']); ?>" name="nama_jenis" id="nama_jenis" type="text" class="form-control" placeholder="Nama Jenis...">
                        <?= form_error('nama_jenis', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="from_jenis">Dari Departemen</label>
                    <div class="col-md-9">
                        <div class="row form-group">
                            <div class="col-md-6">
                                <div class="custom-control custom-radio">
                                    <input <?= $jenis['from_jenis'] == 'KITCHEN' ? 'checked' : ''; ?> <?= set_radio('from_jenis', 'Kitchen'); ?> value="KITCHEN" type="radio" id="Kitchen" name="from_jenis" class="custom-control-input">
                                    <label class="custom-control-label" for="Kitchen">Kitchen</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input <?= $jenis['from_jenis'] == 'BAR' ? 'checked' : ''; ?> <?= set_radio('from_jenis', 'Bar'); ?> value="BAR" type="radio" id="Bar" name="from_jenis" class="custom-control-input">
                                    <label class="custom-control-label" for="Bar">Bar</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input <?= $jenis['from_jenis'] == 'SUPPLIES' ? 'checked' : ''; ?> <?= set_radio('from_jenis', 'Supplies'); ?> value="SUPPLIES" type="radio" id="Supplies" name="from_jenis" class="custom-control-input">
                                    <label class="custom-control-label" for="Supplies">Supplies</label>
                                </div>
                                <?= form_error('from_jenis', '<span class="text-danger small">', '</span>'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-9 offset-md-3">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
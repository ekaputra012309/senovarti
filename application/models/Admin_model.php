<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    public function get($table, $data = null, $where = null)
    {
        if ($data != null) {
            return $this->db->get_where($table, $data)->row_array();
        } else {
            return $this->db->get_where($table, $where)->result_array();
        }
    }

    // getsatuan
    public function getSatuan($table, $column, $value)
    {

        $this->db->where_in($column, $value);
        return $this->db->get_where($table)->result_array();
    }

    public function update($table, $pk, $id, $data)
    {
        $this->db->where($pk, $id);
        return $this->db->update($table, $data);
    }

    public function insert($table, $data, $batch = false)
    {
        return $batch ? $this->db->insert_batch($table, $data) : $this->db->insert($table, $data);
    }

    public function delete($table, $pk, $id)
    {
        return $this->db->delete($table, [$pk => $id]);
    }

    public function delete_d($table, $where)
    {
        return $this->db->delete($table, $where);
    }

    public function getUsers($id)
    {
        /**
         * ID disini adalah untuk data yang tidak ingin ditampilkan.
         * Maksud saya disini adalah
         * tidak ingin menampilkan data user yang digunakan,
         * pada managemen data user
         */
        $this->db->where('id_user !=', $id);
        return $this->db->get('user')->result_array();
    }

    public function getBarang()
    {
        $this->db->join('jenis j', 'b.jenis_id = j.id_jenis');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->order_by('id_barang');
        return $this->db->get('barang b')->result_array();
    }

    // get barang min stok < 10
    public function getBarangMin()
    {
        $this->db->join('jenis j', 'b.jenis_id = j.id_jenis');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->where('stok <', 10);
        $this->db->order_by('id_barang');
        return $this->db->get('barang b')->result_array();
    }

    // get barang join where
    public function getBarangWhere($where)
    {
        $this->db->join('jenis j', 'b.jenis_id = j.id_jenis');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->where('id_brg', $where);
        $this->db->order_by('id_barang');
        return $this->db->get('barang b')->result_array();
    }
    // get invoice where tgl tempo and status
    public function getINVDash($limit = null, $where = null, $range = null)
    {
        $this->db->select('*');
        $this->db->join('supplier s', 'in.supplier_id = s.id_supplier');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($where != null) {
            $this->db->where($where);
        }
        if ($range != null) {
            $this->db->where('tgl_' . ' >=', $range['mulai']);
            $this->db->where('tgl_' . ' <=', $range['akhir']);
        }

        $this->db->order_by('no_inv', 'DESC');
        return $this->db->get('po_invoice in')->result_array();
    }
    // invoice dan po
    public function getPoInvoice($limit = null, $id = null, $status = null)
    {
        $this->db->select('*');
        $this->db->join('supplier s', 'in.supplier_id = s.id_supplier');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id != null) {
            $this->db->where('no_inv', $id);
        }
        if ($status != null) {
            $this->db->where('status', $status);
        }

        $this->db->order_by('no_inv', 'DESC');
        return $this->db->get('po_invoice in')->result_array();
    }
    // invoice dan po detail
    public function getPoInvoiceD($limit = null, $id = null)
    {
        $this->db->select('*');
        $this->db->join('barang b', 'id.id_barang_inv = b.id_brg');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id != null) {
            $this->db->where('d_inv', $id);
        }

        $this->db->order_by('d_inv', 'DESC');
        return $this->db->get('po_invoice_d id')->result_array();
    }

    // po
    public function getPurchaseorder($limit = null, $id = null)
    {
        $this->db->select('*');
        $this->db->join('supplier s', 'po.supplier_id = s.id_supplier');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id != null) {
            $this->db->where('no_po', $id);
        }

        $this->db->order_by('no_po', 'DESC');
        return $this->db->get('purchase_order po')->result_array();
    }
    // po detail
    public function getPurchaseorderD($limit = null, $id = null)
    {
        $this->db->select('*');
        $this->db->join('barang b', 'pod.id_barang_po = b.id_brg');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id != null) {
            $this->db->where('d_po', $id);
        }

        $this->db->order_by('d_po', 'DESC');
        return $this->db->get('purchase_order_d pod')->result_array();
    }
    // tandaterima
    public function getTandaterima($limit = null, $id = null)
    {
        $this->db->select('*');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id != null) {
            $this->db->where('id_tandaterima', $id);
        }

        $this->db->order_by('id_tandaterima', 'DESC');
        return $this->db->get('tandaterima')->result_array();
    }
    // suratjalan
    public function getSuratjalan($limit = null, $id = null)
    {
        $this->db->select('*');
        // $this->db->join('barang b', 'bm.nm_barang = b.id_barang');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id != null) {
            $this->db->where('no_suratjalan', $id);
        }

        $this->db->order_by('no_suratjalan', 'DESC');
        return $this->db->get('suratjalan bm')->result_array();
    }
    // surat jalan detail
    public function getSuratjalanD($limit = null, $id = null)
    {
        $this->db->select('*');
        // $this->db->join('user u', 'bm.user_id = u.id_user');
        $this->db->join('barang b', 'bm.nm_barang = b.id_barang');
        // $this->db->join('po_invoice p', 'bm.d_po_inv = p.no_po_inv');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id != null) {
            $this->db->where('no_suratjalan', $id);
        }

        $this->db->order_by('no_suratjalan', 'DESC');
        return $this->db->get('suratjalan_d bm')->result_array();
    }

    public function getBarangMasuk($limit = null, $id_barang = null, $range = null)
    {
        $this->db->select('*');
        $this->db->join('supplier sp', 'bm.supplier_id = sp.id_supplier');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id_barang != null) {
            $this->db->where('id_barang_masuk', $id_barang);
        }

        if ($range != null) {
            $this->db->where('tanggal_masuk' . ' >=', $range['mulai']);
            $this->db->where('tanggal_masuk' . ' <=', $range['akhir']);
        }

        $this->db->order_by('tanggal_masuk', 'DESC');
        return $this->db->get('barang_masuk bm')->result_array();
    }
    // barang masuk join untuk dashboard
    public function getBMDash($limit = null, $id_barang = null, $range = null)
    {
        $this->db->select('*');
        $this->db->join('barang b', 'bmd.barang_id = b.id_brg');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->join('barang_masuk bm', 'bm.id_barang_masuk = bmd.id_barang_masuk');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id_barang != null) {
            $this->db->where('id_barang_masuk', $id_barang);
        }

        if ($range != null) {
            $this->db->where('tanggal_masuk' . ' >=', $range['mulai']);
            $this->db->where('tanggal_masuk' . ' <=', $range['akhir']);
        }

        $this->db->order_by('tanggal_masuk', 'DESC');
        return $this->db->get('barang_masuk_d bmd')->result_array();
    }
    // barang keluar join untuk dashboard
    public function getBKDash($limit = null, $id_barang = null, $range = null)
    {
        $this->db->select('*');
        $this->db->join('barang b', 'bkd.barang_id = b.id_brg');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->join('barang_keluar bk', 'bk.id_barang_keluar = bkd.id_barang_keluar');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id_barang != null) {
            $this->db->where('id_barang_keluar', $id_barang);
        }

        if ($range != null) {
            $this->db->where('tanggal_keluar' . ' >=', $range['mulai']);
            $this->db->where('tanggal_keluar' . ' <=', $range['akhir']);
        }

        $this->db->order_by('tanggal_keluar', 'DESC');
        return $this->db->get('barang_keluar_d bkd')->result_array();
    }

    public function getBarangKeluar($limit = null, $id_barang = null, $range = null)
    {
        $this->db->select('*');
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($id_barang != null) {
            $this->db->where('id_barang_keluar', $id_barang);
        }
        if ($range != null) {
            $this->db->where('tanggal_keluar' . ' >=', $range['mulai']);
            $this->db->where('tanggal_keluar' . ' <=', $range['akhir']);
        }
        $this->db->order_by('id_barang_keluar', 'DESC');
        return $this->db->get('barang_keluar bk')->result_array();
    }

    public function getBarangWest($limit = null, $range = null)
    {
        $this->db->select('*');
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($range != null) {
            $this->db->where('tanggal_west' . ' >=', $range['mulai']);
            $this->db->where('tanggal_west' . ' <=', $range['akhir']);
        }
        $this->db->order_by('id_barang_west', 'DESC');
        return $this->db->get('barang_west bk')->result_array();
    }

    // barang west detail
    public function getBWdetail($where)
    {
        $this->db->select('*');
        $this->db->where('id_barang_west', $where);
        $this->db->order_by('id_barang_west', 'DESC');
        return $this->db->get('barang_west_d')->result_array();
    }
    // barang masuk detail
    public function getBMdetail($where)
    {
        $this->db->select('*');
        $this->db->join('barang b', 'bmd.barang_id = b.id_brg');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->where('id_barang_masuk', $where);
        $this->db->order_by('id_barang_masuk', 'DESC');
        return $this->db->get('barang_masuk_d bmd')->result_array();
    }
    // barang keluar detail
    public function getBKdetail($where)
    {
        $this->db->select('*');
        $this->db->join('barang b', 'bkd.barang_id = b.id_brg');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->where('id_barang_keluar', $where);
        $this->db->order_by('id_barang_keluar', 'DESC');
        return $this->db->get('barang_keluar_d bkd')->result_array();
    }

    public function getBarangKeluar1($limit = null, $id_barang = null, $range = null)
    {
        $this->db->select('*');
        $this->db->join('user u', 'bk.user_id = u.id_user');
        $this->db->join('barang b', 'bk.barang_id = b.id_barang');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->where('kategori !=', 'eceran');
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($id_barang != null) {
            $this->db->where('id_barang', $id_barang);
        }
        if ($range != null) {
            $this->db->where('tanggal_keluar' . ' >=', $range['mulai']);
            $this->db->where('tanggal_keluar' . ' <=', $range['akhir']);
        }
        $this->db->order_by('id_barang_keluar', 'DESC');
        return $this->db->get('barang_keluar bk')->result_array();
    }

    public function getBarangEceran($limit = null, $id_barang = null, $range = null)
    {
        $this->db->select('*');
        $this->db->join('user u', 'bk.user_id = u.id_user');
        $this->db->join('barang b', 'bk.barang_id = b.id_barang');
        $this->db->join('satuan s', 'b.satuan_id = s.id_satuan');
        $this->db->where('kategori', 'eceran');
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($id_barang != null) {
            $this->db->where('id_barang', $id_barang);
        }
        if ($range != null) {
            $this->db->where('tanggal_keluar' . ' >=', $range['mulai']);
            $this->db->where('tanggal_keluar' . ' <=', $range['akhir']);
        }
        $this->db->order_by('id_barang_keluar', 'DESC');
        return $this->db->get('barang_keluar bk')->result_array();
    }

    // pembukuan
    public function getPembukuan($limit = null, $id_barang = null, $range = null)
    {
        $this->db->select('*');
        if ($limit != null) {
            $this->db->limit($limit);
        }

        if ($id_barang != null) {
            $this->db->where('id_pembukuan', $id_barang);
        }

        if ($range != null) {
            $this->db->where('tanggal' . ' >=', $range['mulai']);
            $this->db->where('tanggal' . ' <=', $range['akhir']);
        }

        $this->db->order_by('tanggal', 'ASC');
        return $this->db->get('pembukuan')->result_array();
    }

    public function getMax($table, $field, $kode = null)
    {
        $this->db->select_max($field);
        if ($kode != null) {
            $this->db->like($field, $kode, 'after');
        }
        return $this->db->get($table)->row_array()[$field];
    }

    public function count($table)
    {
        return $this->db->count_all($table);
    }

    public function sum($table, $field)
    {
        $this->db->select_sum($field);
        return $this->db->get($table)->row_array()[$field];
    }

    // sum kardus
    public function sum1($table, $field)
    {
        $this->db->select_sum($field);
        $this->db->where('satuan_id', 1);
        return $this->db->get($table)->row_array()[$field];
    }

    // sum kg
    public function sum2($table, $field)
    {
        $this->db->select_sum($field);
        $this->db->where('satuan_id', 2);
        return $this->db->get($table)->row_array()[$field];
    }

    public function min($table, $field, $min)
    {
        $field = $field . ' <=';
        $this->db->where($field, $min);
        return $this->db->get($table)->result_array();
    }

    public function chartBarangMasuk($bulan)
    {
        $like = 'T-BM-' . date('y') . $bulan;
        $this->db->like('id_barang_masuk', $like, 'after');
        return count($this->db->get('barang_masuk')->result_array());
    }

    public function chartBarangKeluar($bulan)
    {
        $like = 'T-BK-' . date('y') . $bulan;
        $this->db->like('id_barang_keluar', $like, 'after');
        return count($this->db->get('barang_keluar')->result_array());
    }

    public function chartBarangWest($bulan)
    {
        $like = 'T-BW-' . date('y') . $bulan;
        $this->db->like('id_barang_west', $like, 'after');
        return count($this->db->get('barang_west')->result_array());
    }

    public function laporan($table, $mulai, $akhir)
    {
        $tgl = $table == 'barang_masuk' ? 'tanggal_masuk' : 'tanggal_keluar';
        $this->db->where($tgl . ' >=', $mulai);
        $this->db->where($tgl . ' <=', $akhir);
        return $this->db->get($table)->result_array();
    }

    public function cekStok($id)
    {
        $this->db->join('satuan s', 'b.satuan_id=s.id_satuan');
        return $this->db->get_where('barang b', ['id_barang' => $id])->row_array();
    }

    public function getLog()
    {
        $this->db->join('user s', 'b.log_user_id = s.id_user');
        $this->db->order_by('log_id', 'DESC');
        return $this->db->get('log_user b')->result_array();
    }
}

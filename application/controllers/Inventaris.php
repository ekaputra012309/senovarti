<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inventaris extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = "Inventaris";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['data_t'] = $this->admin->get('inventaris');
        $this->template->load('templates/dashboard', 'inventaris/data', $data);
    }

    private function _validasi()
    {
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('vendor', 'Nama Vendor', 'required|trim');
        $this->form_validation->set_rules('no_doc', 'Nomor Dokumen', 'required|trim');
        $this->form_validation->set_rules('nama_item', 'Nama Item', 'required|trim');
        $this->form_validation->set_rules('spesifikasi', 'Spesifikasi', 'required|trim');
        $this->form_validation->set_rules('qty', 'Kuantitas', 'required|trim|numeric');
        $this->form_validation->set_rules('harga', 'Harga', 'required|trim|numeric');
        $this->form_validation->set_rules('metode', 'Metode', 'required|trim');
        $this->form_validation->set_rules('biaya_dp', 'DP', 'required|trim|numeric');
        $this->form_validation->set_rules('jangka_waktu', 'Jangka Waktu', 'required|trim');
        $this->form_validation->set_rules('biaya_perbulan', 'Biaya Per Bulan', 'required|trim|numeric');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required|trim');
        $this->form_validation->set_rules('total', 'Total', 'required|trim|numeric');
    }

    public function add()
    {
        $this->_validasi();
        if ($this->form_validation->run() == false) {
            $data['title'] = "Inventaris";
            $data['about'] = $this->admin->get('about', ['id' => '1']);
            $this->template->load('templates/dashboard', 'inventaris/add', $data);
        } else {
            $config['upload_path'] = './assets/img/inventaris';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['encrypt_name']     = TRUE;
            $config['max_size'] = 2048;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('fileup')) {
                $data['upload_error'] = $this->upload->display_errors();
                $data['title'] = "Tanda Terima";
                $data['about'] = $this->admin->get('about', ['id' => '1']);
                $this->template->load('templates/dashboard', 'inventaris/add', $data);
            } else {
                $input = $this->input->post(null, true);
                $file_data = $this->upload->data();
                $input['fileup'] = $file_data['file_name'];
                // echo json_encode($input);
                $insert = $this->admin->insert('inventaris', $input);

                if ($insert) {
                    set_pesan('data berhasil disimpan');
                    redirect('inventaris');
                } else {
                    set_pesan('gagal menyimpan data');
                    redirect('inventaris/add');
                }
            }
        }
    }

    // public function edit($getId)
    // {
    //     $id = encode_php_tags($getId);
    //     $this->_validasi();

    //     if ($this->form_validation->run() == false) {
    //         $data['title'] = "Tanda Terima";
    //         $data['about'] = $this->admin->get('about', ['id' => '1']);
    //         $data['td'] = $this->admin->get('inventaris', ['id_inventaris' => $id]);
    //         $this->template->load('templates/dashboard', 'inventaris/edit', $data);
    //     } else {
    //         $input = $this->input->post(null, true);
    //         $user_data = array(
    //             'dari' => $input['dari'],
    //             'kepada' => $input['kepada'],
    //             'tanggal' => $input['tanggal'],
    //             'nominal' => $input['nominal'],
    //             'keterangan' => $input['keterangan']
    //         );
    //         if ($_FILES['fileup']['name'] !== '') {
    //             $config['upload_path'] = './assets/img/inventaris';
    //             $config['allowed_types'] = 'gif|jpg|jpeg|png';
    //             $config['encrypt_name']     = TRUE;
    //             $config['max_size'] = 2048;

    //             $this->load->library('upload', $config);

    //             if (!$this->upload->do_upload('fileup')) {
    //                 $data['upload_error'] = $this->upload->display_errors();
    //                 // $this->load->view('user_form', $data);
    //                 return;
    //             }
    //             $tandaT = $this->db->get_where('inventaris', array('id_inventaris' => $getId))->row();
    //             $file_path = FCPATH . 'assets/img/inventaris/' . $tandaT->fileup;
    //             if (file_exists($file_path)) {
    //                 unlink($file_path);
    //             }
    //             $file_data = $this->upload->data();
    //             $user_data['fileup'] = $file_data['file_name'];
    //         }
    //         $update = $this->admin->update('inventaris', 'id_inventaris', $id, $user_data);
    //         if ($update) {
    //             set_pesan('data berhasil disimpan');
    //             redirect('inventaris');
    //         } else {
    //             set_pesan('data gagal disimpan', false);
    //             redirect('inventaris/add');
    //         }
    //     }
    // }

    public function delete($getId)
    {
        $id = encode_php_tags($getId);
        $tandaT = $this->db->get_where('inventaris', array('id_inventaris' => $getId))->row();
        $file_path = FCPATH . 'assets/img/inventaris/' . $tandaT->fileup;
        if ($this->admin->delete('inventaris', 'id_inventaris', $id)) {
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('inventaris');
    }

    // public function print_($id)
    // {
    //     $this->load->helper(['terbilang', 'tgl_indo']);
    //     // get data table
    //     $data = $this->admin->getinventaris(null, $id);
    //     $dataA = $this->admin->get('about', ['id' => '1']);

    //     $this->load->library('PDF_MC_Table');

    //     $pdf = new PDF_MC_Table();
    //     $pdf->AddPage('L', 'A4');

    //     $pdf->SetFont('Arial', 'B', 14);

    //     $pdf->Cell(115, 5, ' ', 0, 0);
    //     $pdf->Image('assets/img/avatar/' . $dataA['foto'] . '', 10, 10, 0, 30);
    //     $pdf->Ln(20);

    //     foreach ($data as $b) {
    //         // $imageUrl = 'assets/img/inventaris/' . $b['fileup'];
    //         // $imageSize = getimagesize($imageUrl);
    //         // if ($imageSize !== false) {
    //         //     $width = $imageSize[0];
    //         //     $height = $imageSize[1];
    //         //     if ($width > $height) {
    //         //         $pdf->ImageWithBorder('assets/img/inventaris/' . $b['fileup'] . '', 150, 55, 0, 40);
    //         //     } else {
    //         //         $pdf->ImageWithBorder('assets/img/inventaris/' . $b['fileup'] . '', 200, 55, 0, 50);
    //         //     }
    //         // }
    //         $pdf->SetFont('Arial', 'B', 18);
    //         $pdf->Cell(275, 15, 'TANDA TERIMA', 'B', 1, 'C'); //end of line
    //         $pdf->Ln(5);
    //         //summary
    //         $pdf->SetWidths(array(222));
    //         $pdf->SetLineHeight(5);
    //         $pdf->SetAligns(array('L'));
    //         $pdf->SetFont('Arial', '', 16);

    //         $pdf->Cell(50, 5, 'Dari', 0, 0, 'L');
    //         $pdf->Cell(3, 5, ':', 0, 0, 'L');
    //         $pdf->RowN(array($b['dari']));
    //         $pdf->Ln();
    //         $pdf->Cell(50, 5, 'Kepada', 0, 0, 'L');
    //         $pdf->Cell(3, 5, ':', 0, 0, 'L');
    //         $pdf->RowN(array($b['kepada']));
    //         $pdf->Ln();
    //         $pdf->Cell(50, 5, 'Hari/Tanggal', 0, 0, 'L');
    //         $pdf->Cell(3, 5, ':', 0, 0, 'L');
    //         $pdf->RowN(array(longdate_indo($b['tanggal'])));
    //         $pdf->Ln();
    //         $pdf->Cell(50, 5, 'Nomilal', 0, 0, 'L');
    //         $pdf->Cell(3, 5, ':', 0, 0, 'L');
    //         $pdf->RowN(array(number_format($b['nominal'], 0, ',', '.')));
    //         $pdf->Ln();
    //         $pdf->Cell(50, 5, 'Terbilang', 0, 0, 'L');
    //         $pdf->Cell(3, 5, ':', 0, 0, 'L');
    //         $pdf->RowN(array(ucfirst(terbilang($b['nominal']))));
    //         $pdf->Ln();
    //         $pdf->Cell(50, 5, 'Keterangan', 0, 0, 'L');
    //         $pdf->Cell(3, 5, ':', 0, 0, 'L');
    //         $pdf->RowN(array($b['keterangan']));
    //         $pdf->Ln();

    //         $pdf->Cell(275, 5, '', 'B', 1, 'C'); //end of line
    //         $pdf->Ln();

    //         $pdf->Cell(30, 5, '', 0, 0);
    //         $pdf->Cell(50, 5, 'Yang menyerahkan,', 0, 0, 'C');
    //         $pdf->Cell(100, 5, '', 0, 0);
    //         $pdf->Cell(50, 5, 'Yang menerima,', 0, 1, 'C');
    //         $pdf->Ln(30);
    //         $pdf->Cell(30, 5, '', 0, 0);
    //         $pdf->Cell(50, 5, '........................', 0, 0, 'C');
    //         $pdf->Cell(100, 5, '', 0, 0);
    //         $pdf->Cell(50, 5, '........................', 0, 1, 'C');
    //     }

    //     $file_name = $id;
    //     $pdf->Output('I', $file_name);
    // }
}

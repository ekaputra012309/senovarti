<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barangkeluar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = "Barang Keluar";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['barangkeluar'] = $this->admin->getBarangkeluar();
        // if (!is_owner()) {
        //     $this->load->view('404');
        // } else {
        $this->template->load('templates/dashboard', 'barang_keluar/data', $data);
        // }
    }

    public function add()
    {
        $data['title'] = "Barang Keluar";
        $data['about'] = $this->admin->get('about', ['id' => '1']);

        // Mendapatkan dan men-generate kode transaksi barang keluar
        $kode = 'BK' . userdata('kd_user') . '-' . date('ymd');
        $kode_terakhir = $this->admin->getMax('barang_keluar', 'id_barang_keluar', $kode);
        $kode_tambah = substr($kode_terakhir, -5, 5);
        $kode_tambah++;
        $number = str_pad($kode_tambah, 5, '0', STR_PAD_LEFT);
        $data['id_barang_keluar'] = $kode . $number;
        $data['barangkeluar_d'] = $this->admin->getBKdetail($data['id_barang_keluar']);
        $this->template->load('templates/dashboard', 'barang_keluar/add', $data);
    }

    public function add_p()
    {
        $this->form_validation->set_rules('tanggal_keluar', 'Tanggal keluar', 'required|trim');
        $this->form_validation->set_rules('pajakbkd', 'Pajak', 'required|trim');
        $this->form_validation->set_rules('subtotal', 'Sub Total', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->add();
        } else {
            $input = $this->input->post(null, true);
            $dataIn = array(
                'id_barang_keluar' => $input['id_barang_keluar'],
                'tanggal_keluar' => $input['tanggal_keluar'],
                'pajak_bkd' => $input['pajak_bkd'],
                'total' => $input['total']
            );
            $insert = $this->admin->insert('barang_keluar', $dataIn);
            // echo $this->db->last_query();
            if ($insert) {
                set_pesan('data berhasil disimpan.');
                redirect('barangkeluar');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('barangkeluar/add');
            }
        }
    }

    public function add_d()
    {
        $data['title'] = "Barang Keluar";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        if (is_kitchen()) {
            $this->db->where('from_jenis', 'KITCHEN');
            $data['barang'] = $this->admin->getBarang();
        } else if (is_bar()) {
            $this->db->where('from_jenis', 'BAR');
            $data['barang'] = $this->admin->getBarang();
        } else if (is_gudang()) {
            $this->db->where('from_jenis', 'SUPPLIES');
            $data['barang'] = $this->admin->getBarang();
        } else {
            $data['barang'] = $this->admin->getBarang();
        }


        if (is_gudang()) {
            $value = array('3', '17', '20', '22');
            $data['satuan'] = $this->admin->getSatuan('satuan', 'id_satuan', $value);
        } else if (is_kitchen()) {
            $value = array('12', '22');
            $data['satuan'] = $this->admin->getSatuan('satuan', 'id_satuan', $value);
        } else if (is_bar()) {
            $value = array('4', '12', '22');
            $data['satuan'] = $this->admin->getSatuan('satuan', 'id_satuan', $value);
        } else {
            $data['satuan'] = $this->admin->get('satuan');
        }
        // echo $this->db->last_query();
        // Mendapatkan dan men-generate kode transaksi barang keluar
        $kode = 'BK' . userdata('kd_user') . '-' . date('ymd');
        $kode_terakhir = $this->admin->getMax('barang_keluar', 'id_barang_keluar', $kode);
        $kode_tambah = substr($kode_terakhir, -5, 5);
        $kode_tambah++;
        $number = str_pad($kode_tambah, 5, '0', STR_PAD_LEFT);
        $data['id_barang_keluar'] = $kode . $number;

        $this->template->load('templates/dashboard', 'barang_keluar/add_d', $data);
    }

    public function add_dp()
    {
        $this->form_validation->set_rules('barang_id', 'Barang', 'required');
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
        $input = $this->input->post('barang_id', true);
        $stok = $this->admin->get('barang', ['id_brg' => $input])['stok'];
        $stok_valid = $stok + 1;

        $this->form_validation->set_rules(
            'jumlah_keluar',
            'Jumlah keluar',
            "required|trim|numeric|greater_than[0]|less_than_equal_to[{$stok}]",
            [
                'less_than_equal_to' => "Jumlah keluar tidak boleh lebih dari {$stok}"
            ]
        );
        $this->form_validation->set_rules('satuan_k', 'Satuan', 'required');
        $this->form_validation->set_rules('stok', 'Stok', 'required|trim');
        $this->form_validation->set_rules('ukuran', 'Ukuran', 'required|trim');
        $this->form_validation->set_rules('harga_keluar', 'Harga keluar', 'required|trim|numeric|greater_than_equal_to[0]');
        $this->form_validation->set_rules('total', 'Total', 'required|trim|numeric');
        if ($this->form_validation->run() == false) {
            $this->add_d();
        } else {
            $input = $this->input->post(null, true);
            // echo $input;
            $dataIn = array(
                'id_barang_keluar' => $input['id_barang_keluar'],
                'barang_id' => $input['barang_id'],
                'jumlah_keluar' => $input['jumlah_keluar'],
                'satuan_k' => $input['satuan_k'],
                'ukuran' => $input['ukuran'],
                'harga_keluar' => $input['harga_keluar'],
                // 'pajak_bmd' => $input['pajak_bmd'],
                'total' => $input['total']
            );
            $insert = $this->admin->insert('barang_keluar_d', $dataIn);
            echo $this->db->last_query();
            if ($insert) {
                // set_pesan('Add barang berhasil.');
                redirect('barangkeluar/add');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('barangkeluar/add_d');
            }
        }
    }

    public function getBrg($id)
    {
        $data = $this->admin->getBarangWhere($id);
        echo json_encode($data);
    }

    public function delete($getId)
    {
        $id = encode_php_tags($getId);
        if ($this->admin->delete('barang_keluar', 'id_barang_keluar', $id)) {
            $this->admin->delete('barang_keluar_d', 'id_barang_keluar', $id);
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('barangkeluar');
    }

    public function delete_d($getId)
    {
        $id = encode_php_tags($getId);
        if ($this->admin->delete('barang_keluar_d', 'id_bkd', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('barangkeluar/add');
    }

    public function cetak($id)
    {
        $data['title'] = "Transaksi Barang Keluar";
        $data['page'] = 'size:  21cm 29.7cm;';
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['barangkeluar'] = $this->admin->getBarangkeluar(null, $id, null);
        $this->template->load('print', 'barang_keluar/cetak', $data);
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
    }

    public function index()
    {
        $data['title'] = "Dashboard";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['barang'] = $this->admin->count('barang');
        $data['barang_masuk'] = $this->admin->count('barang_masuk');
        $data['barang_keluar'] = $this->admin->count('barang_keluar');
        $data['supplier'] = $this->admin->count('supplier');
        $data['user'] = $this->admin->count('user');
        $data['stok'] = $this->admin->sum1('barang', 'stok');
        $data['stok2'] = $this->admin->sum2('barang', 'stok');
        $data['barang_min'] = $this->admin->getBarangMin();
        $data['tempo'] = $this->admin->getINVDash(30, ['tgl_tempo < ' => date('Y-m-d'), 'status' => '0'], ['akhir' => date('Y-m-d'), 'mulai' => date("Y-m-d", strtotime(date('Y-m-d') . " -7 days"))]);
        $data['transaksi'] = [
            'barang_masuk' => $this->admin->getBMDash(30, null, ['akhir' => date('Y-m-d'), 'mulai' => date("Y-m-d", strtotime(date('Y-m-d') . " -7 days"))]),
            'barang_keluar' => $this->admin->getBKDash(30, null, ['akhir' => date('Y-m-d'), 'mulai' => date("Y-m-d", strtotime(date('Y-m-d') . " -7 days"))]),
            'barang_west' => $this->admin->getBarangWest(30)
        ];

        // Line Chart
        $bln = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        $data['cbm'] = [];
        $data['cbk'] = [];
        $data['cbw'] = [];

        foreach ($bln as $b) {
            $data['cbm'][] = $this->admin->chartBarangMasuk($b);
            $data['cbk'][] = $this->admin->chartBarangKeluar($b);
            $data['cbw'][] = $this->admin->chartBarangWest($b);
        }

        $this->template->load('templates/dashboard', 'dashboard', $data);
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();
        if (!is_owner()) {
            redirect('dashboard');
        }

        $this->load->model('Admin_model', 'admin');
        $this->load->library('form_validation');
    }

    private function _validasi()
    {
        $this->form_validation->set_rules('nama_perusahaan', 'Nama Perusahaan', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim');
    }

    private function _config()
    {
        $config['upload_path']      = "./assets/img/avatar";
        $config['allowed_types']    = 'gif|jpg|jpeg';
        $config['encrypt_name']     = TRUE;
        $config['max_size']         = '2048';

        $this->load->library('upload', $config);
    }

    public function index()
    {
        $this->_validasi();
        $this->_config();

        if ($this->form_validation->run() == false) {
            $data['title'] = "About";
            $data['about'] = $this->admin->get('about', ['id' => '1']);
            $this->template->load('templates/dashboard', 'about', $data);
        } else {
            $input = $this->input->post(null, true);
            if ($_FILES['foto']['name'] !== '') {
                $config['upload_path'] = './assets/img/avatar';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['encrypt_name']     = TRUE;
                $config['max_size'] = 2048;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('foto')) {
                    $data['upload_error'] = $this->upload->display_errors();
                    return;
                }
                $about = $this->db->get_where('about', array('id' => '1'))->row();
                $file_path = FCPATH . 'assets/img/avatar/' . $about->foto;
                if (file_exists($file_path)) {
                    unlink($file_path);
                }
                $file_data = $this->upload->data();
                $input['foto'] = $file_data['file_name'];
            }
            $update = $this->admin->update('about', 'id', '1', $input);
            if ($update) {
                set_pesan('data berhasil disimpan');
                redirect('about');
            } else {
                set_pesan('data gagal disimpan', false);
                redirect('about');
            }
        }
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barangmasuk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = "Barang Masuk";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['barangmasuk'] = $this->admin->getBarangMasuk();
        $this->template->load('templates/dashboard', 'barang_masuk/data', $data);
    }

    public function add()
    {
        $data['title'] = "Barang Masuk";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['supplier'] = $this->admin->get('supplier');

        // Mendapatkan dan men-generate kode transaksi barang masuk
        $kode = 'BM' . userdata('kd_user') . '-' . date('ymd');
        $kode_terakhir = $this->admin->getMax('barang_masuk', 'id_barang_masuk', $kode);
        $kode_tambah = substr($kode_terakhir, -5, 5);
        $kode_tambah++;
        $number = str_pad($kode_tambah, 5, '0', STR_PAD_LEFT);
        $data['id_barang_masuk'] = $kode . $number;
        $data['barangmasuk_d'] = $this->admin->getBMdetail($data['id_barang_masuk']);
        $this->template->load('templates/dashboard', 'barang_masuk/add', $data);
    }

    public function add_p()
    {
        $this->form_validation->set_rules('tanggal_masuk', 'Tanggal Masuk', 'required|trim');
        $this->form_validation->set_rules('supplier_id', 'Supplier', 'required|trim');
        $this->form_validation->set_rules('pajakbmd', 'Pajak', 'required|trim');
        $this->form_validation->set_rules('subtotal', 'Sub Total', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->add();
        } else {
            $input = $this->input->post(null, true);
            $dataIn = array(
                'id_barang_masuk' => $input['id_barang_masuk'],
                'tanggal_masuk' => $input['tanggal_masuk'],
                'supplier_id' => $input['supplier_id'],
                'pajak_bmd' => $input['pajak_bmd'],
                'total' => $input['total']
            );
            $insert = $this->admin->insert('barang_masuk', $dataIn);
            // echo $this->db->last_query();
            if ($insert) {
                set_pesan('data berhasil disimpan.');
                redirect('barangmasuk');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('barangmasuk/add');
            }
        }
    }

    public function add_d()
    {
        $this->form_validation->set_rules('barang_id', 'Barang', 'required');
        $this->form_validation->set_rules('jumlah_masuk', 'Jumlah Masuk', 'required|trim|numeric|greater_than[0]');
        $this->form_validation->set_rules('qty_pack', 'Jumlah Pack', 'required|trim|numeric');
        $this->form_validation->set_rules('harga_masuk', 'Harga Masuk', 'required|trim|numeric|greater_than_equal_to[0]');
        $this->form_validation->set_rules('total', 'Total', 'required|trim|numeric');

        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
        $this->form_validation->set_rules('satuan', 'Satuan', 'required');
        $this->form_validation->set_rules('weight', 'Berat', 'required');

        if ($this->form_validation->run() == false) {
            $data['title'] = "Barang Masuk";
            $data['about'] = $this->admin->get('about', ['id' => '1']);
            $data['barang'] = $this->admin->getBarang();
            $data['satuan'] = $this->admin->get('satuan');
            // Mendapatkan dan men-generate kode transaksi barang masuk
            $kode = 'BM' . userdata('kd_user') . '-' . date('ymd');
            $kode_terakhir = $this->admin->getMax('barang_masuk', 'id_barang_masuk', $kode);
            $kode_tambah = substr($kode_terakhir, -5, 5);
            $kode_tambah++;
            $number = str_pad($kode_tambah, 5, '0', STR_PAD_LEFT);
            $data['id_barang_masuk'] = $kode . $number;

            $this->template->load('templates/dashboard', 'barang_masuk/add_d', $data);
        } else {
            $input = $this->input->post(null, true);
            $dataIn = array(
                'id_barang_masuk' => $input['id_barang_masuk'],
                'barang_id' => $input['barang_id'],
                'jumlah_masuk' => $input['jumlah_masuk'],
                'ukuran' => $input['ukuran'],
                'qty_pack' => $input['qty_pack'],
                'harga_masuk' => $input['harga_masuk'],
                // 'pajak_bmd' => $input['pajak_bmd'],
                'total' => $input['total']
            );
            $insert = $this->admin->insert('barang_masuk_d', $dataIn);
            if ($insert) {
                // set_pesan('Add barang berhasil.');
                redirect('barangmasuk/add');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('barangmasuk/add_d');
            }
        }
    }

    public function getBrg($id)
    {
        $data = $this->admin->getBarangWhere($id);
        echo json_encode($data);
    }

    public function delete($getId)
    {
        $id = encode_php_tags($getId);
        if ($this->admin->delete('barang_masuk', 'id_barang_masuk', $id)) {
            $this->admin->delete('barang_masuk_d', 'id_barang_masuk', $id);
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('barangmasuk');
    }

    public function delete_d($getId)
    {
        $id = encode_php_tags($getId);
        if ($this->admin->delete('barang_masuk_d', 'id_bmd', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('barangmasuk/add');
    }

    public function cetak($id)
    {
        $data['title'] = "Transaksi Barang Masuk";
        $data['page'] = 'size:  21cm 29.7cm;';
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['barangmasuk'] = $this->admin->getBarangMasuk(null, $id, null);
        $this->template->load('print', 'barang_masuk/cetak', $data);
    }
}

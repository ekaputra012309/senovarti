<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Invoice extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = "INVOICE";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['datainvoice'] = $this->admin->get('po_invoice');
        $this->template->load('templates/dashboard', 'po_invoice/data_inv', $data);
    }

    // page input po
    public function add()
    {
        $data['title'] = "Tambah Invoice";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['supplier'] = $this->admin->get('supplier');
        $kode = 'INV-' . date('Y-');
        $kode_terakhir = $this->admin->getMax('po_invoice', 'no_inv', $kode);
        $kode_tambah = substr($kode_terakhir, -4, 4);
        $kode_tambah++;
        $number = str_pad($kode_tambah, 4, '0', STR_PAD_LEFT);
        $data['no_inv'] = $kode . $number;
        $data['invoice_d'] = $this->admin->getPoInvoiceD(null, $data['no_inv']);
        $this->template->load('templates/dashboard', 'po_invoice/add_inv', $data);
    }

    // page add detail po
    public function add_d()
    {
        $this->form_validation->set_rules('id_barang_inv', 'Barang', 'required');
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
        $this->form_validation->set_rules('qty', 'Kuantitas', 'required|trim|numeric|greater_than[0]');
        $this->form_validation->set_rules('harga_d', 'Harga', 'required|trim|numeric');
        $this->form_validation->set_rules('diskon', 'Diskon', 'required|trim|numeric');
        $this->form_validation->set_rules('pajak_d', 'Pajak', 'required|trim|numeric');
        $this->form_validation->set_rules('total', 'Total', 'required|trim|numeric');
        if ($this->form_validation->run() == false) {
            $data['title'] = "Invoice Detail";
            $data['about'] = $this->admin->get('about', ['id' => '1']);
            $data['barang'] = $this->admin->getBarang();
            $kode = 'INV-' . date('Y-');
            $kode_terakhir = $this->admin->getMax('po_invoice', 'no_inv', $kode);
            $kode_tambah = substr($kode_terakhir, -4, 4);
            $kode_tambah++;
            $number = str_pad($kode_tambah, 4, '0', STR_PAD_LEFT);
            $data['no_inv'] = $kode . $number;
            $this->template->load('templates/dashboard', 'po_invoice/add_invd', $data);
        } else {
            $input = $this->input->post(null, true);
            $dataIn = array(
                'd_inv' => $input['d_inv'],
                'id_barang_inv' => $input['id_barang_inv'],
                'qty' => $input['qty'],
                'harga_d' => $input['harga_d'],
                'diskon' => $input['diskon'],
                'pajak_d' => $input['pajak_d'],
                'total' => $input['total']
            );
            // echo json_encode($dataIn);
            $insert = $this->admin->insert('po_invoice_d', $dataIn);
            if ($insert) {
                set_pesan('Add barang berhasil.');
                redirect('invoice/add');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('invoice/add_d');
            }
        }
    }

    // proses insert di page input invoice
    public function add_p()
    {
        $this->form_validation->set_rules('tgl_', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('tgl_faktur', 'Tanggal Tuker Faktur', 'required|trim');
        $this->form_validation->set_rules('tgl_tempo', 'Tanggal Jatuh Tempo', 'required|trim');
        $this->form_validation->set_rules('supplier_id', 'Supplier', 'required|trim');
        $this->form_validation->set_rules('catatan', 'Catatan', 'required|trim');
        $this->form_validation->set_rules('subtotal', 'Sub Total', 'required|trim');
        $this->form_validation->set_rules('jumlah_tertagih', 'Jumlah Tertagih', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->add();
        } else {
            $config['upload_path'] = './assets/img/invoice';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['encrypt_name']     = TRUE;
            $config['max_size'] = 2048;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('fileup')) {
                $data['upload_error'] = $this->upload->display_errors();
                $data['title'] = "Tanda Terima";
                $data['about'] = $this->admin->get('about', ['id' => '1']);
                $this->template->load('templates/dashboard', 'tandaterima/add', $data);
            } else {
                $input = $this->input->post(null, true);
                if ($input['tgl_tempo'] == '7') {
                    $tmpo = date("Y-m-d", strtotime($input['tgl_'] . " +7 days"));
                } elseif ($tp['tgl_tempo'] == '14') {
                    $tmpo = date("Y-m-d", strtotime($input['tgl_'] . " +14 days"));
                } else {
                    $tmpo = date("Y-m-d", strtotime($input['tgl_'] . " +30 days"));
                }
                $input['tgl_tempo'] = $tmpo;
                $file_data = $this->upload->data();
                $input['fileup'] = $file_data['file_name'];
                $insert = $this->admin->insert('po_invoice', $input);
                echo $this->db->last_query();
                if ($insert) {
                    set_pesan('data berhasil disimpan.');
                    redirect('invoice');
                } else {
                    set_pesan('Opps ada kesalahan!');
                    redirect('invoice/add');
                }
            }
        }
    }

    public function delete($getId)
    {
        $id = encode_php_tags($getId);
        if ($this->admin->delete('po_invoice', 'no_inv', $id)) {
            $this->admin->delete('po_invoice_d', 'd_inv', $id);
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('invoice');
    }

    public function del_d($getId)
    {
        $id = encode_php_tags($getId);
        if ($this->admin->delete_d('po_invoice_d', ['idinv' => $id])) {
            // set_pesan('data berhasil dihapus.');
        } else {
            // set_pesan('data gagal dihapus.', false);
        }
        redirect('invoice/add');
    }

    public function print_($id)
    {
        // get data table
        $data = $this->admin->getPoInvoice(null, $id);
        $dat2 = $this->admin->getPoInvoiceD(null, $id);

        $this->load->library('PDF_MC_Table');

        $pdf = new PDF_MC_Table();
        $pdf->AddPage('P', 'A4');

        $pdf->SetFont('Arial', 'B', 14);

        $pdf->Cell(115, 5, ' ', 0, 0);
        $pdf->Image('assets/img/logosksejahtera.jpeg', 10, 10, 30);
        $pdf->Cell(74, 5, 'INVOICE', 0, 1, 'R'); //end of line
        $pdf->Ln(3);

        $pdf->SetFont('Arial', '', 12);

        foreach ($data as $d) {
            $noinv = explode('-', $d['no_inv']);
            $nama = explode(':', $d['data']);

            $nm = array('CV. Ryo Multi Makmur', $nama[0]);
            $al = array('Jln Kali Baru Barat 2 A NO:23 RT 003/010, ' . "\n" . 'Kel: Kali Baru, Kec: Cilincing Jakarta Utara, ' . "\n" . 'Kota Jakarta Utara, DKI Jakarta, Indonesia', $nama[1]);
            $tp = array('Telp: +62 858-1352-7166 ', $nama[2]);
            $em = array('Email: rmmsupport@sksejahtera.com', $nama[3]);

            $pdf->Cell(115, 5, ' ', 0, 0);
            $pdf->Cell(36, 5, 'Referensi', 0, 0, 'R');
            $pdf->Cell(38, 5, $noinv[0] . '/' . $noinv[1] . '/' . $noinv[2], 0, 1, 'R'); //end of line

            $pdf->Cell(115, 5, ' ', 0, 0);
            $pdf->Cell(36, 5, 'Tanggal', 0, 0, 'R');
            $date = date_create($d['tgl_']);
            $pdf->Cell(38, 5, date_format($date, "d/m/Y"), 0, 1, 'R'); //end of line

            $pdf->Cell(115, 5, ' ', 0, 0);
            $pdf->Cell(36, 5, 'Tgl. Jatuh Tempo', 0, 0, 'R');
            $datet = date_create($d['tgl_tempo']);
            $pdf->Cell(38, 5, date_format($datet, "d/m/Y"), 0, 1, 'R'); //end of line

            $pdf->Cell(115, 5, ' ', 0, 0);
            $pdf->Cell(36, 5, 'No.NPWP', 0, 0, 'R');
            $pdf->Cell(38, 5, '355391301416000', 0, 1, 'R'); //end of line
            $pdf->Ln(4);
            $pdf->Line(10, 41, 199, 41);

            $pdf->SetWidths(array(99, 90));
            $pdf->SetLineHeight(5);
            $pdf->SetAligns(array('', ''));
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(99, 5, 'Info Perusahaan', 0, 0);
            $pdf->Cell(90, 5, 'Tagihan Untuk', 0, 1); //end of line
            $pdf->Ln(3);
            $pdf->Line(10, 48, 199, 48);

            $pdf->SetFont('Arial', '', 12);
            $pdf->RowN(array($nm[0], $nm[1]));
            $pdf->RowN(array($al[0], $al[1]));
            $pdf->RowN(array($tp[0], 'Telp: ' . $tp[1]));
            $pdf->RowN(array($em[0], 'Email: ' . $em[1]));
            $pdf->Ln();
        }

        $pdf->SetWidths(array(65, 15, 30, 15, 20, 44));
        $pdf->SetLineHeight(5);
        $pdf->SetAligns(array('', 'R', 'R', 'R', 'C', 'R'));
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(65, 5, 'Produk', 1, 0);
        $pdf->Cell(15, 5, 'Qty', 1, 0, 'C');
        $pdf->Cell(30, 5, 'Harga', 1, 0, 'C');
        $pdf->Cell(15, 5, 'Disc', 1, 0, 'C');
        $pdf->Cell(20, 5, 'Pajak', 1, 0, 'C');
        $pdf->Cell(44, 5, 'Jumlah', 1, 1, 'C'); //end of line

        $pdf->SetFont('Arial', '', 12);
        foreach ($dat2 as $v) {
            $pdf->Row(array(
                $v['nama_barang'] . ' - ' . $v['deskripsi'],
                $v['qty'],
                number_format($v['harga_d'], 2, ',', '.'),
                $v['diskon'] . ' %',
                $v['pajak_d'] . ' %',
                number_format($v['jumlah_d'], 2, ',', '.')
            ));
        }
        $pdf->Ln();

        foreach ($data as $b) {
            //summary
            $pdf->Cell(94, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Subtotal', 0, 0, 'R');
            $pdf->Cell(70, 5, 'Rp ' . number_format($b['subtotal'], 2, ',', '.'), 0, 1, 'R'); //end of line

            $pdf->Cell(94, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Pajak', 0, 0, 'R');
            $pdf->Cell(70, 5, 'Rp ' . '(' . number_format($b['pajak'], 2, ',', '.') . ')', 0, 1, 'R'); //end of line

            $pdf->Cell(94, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Total', 0, 0, 'R');
            $pdf->Cell(70, 5, 'Rp ' . number_format($b['total'], 2, ',', '.'), 0, 1, 'R'); //end of line

            $pdf->Cell(74, 5, '', 0, 0);
            $pdf->Cell(45, 7, 'Pajak Inclusive ' . $b['pajak_inc'] . '%', 'B', 0, 'R');
            $cc = ((100 / (100 + $b['pajak_inc'])) * $b['total']) * $b['pajak_inc'] / 100;
            $pdf->Cell(70, 7, 'Rp ' . number_format($cc, 2, ',', '.'), 'B', 1, 'R'); //end of line

            $pdf->Ln(2);
            $pdf->Cell(94, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Jumlah Tertagih:', 0, 0, 'R');
            $pdf->Cell(70, 5, 'Rp ' . number_format($b['jumlah_tagihan'], 2, ',', '.'), 0, 1, 'R'); //end of line
            $pdf->Ln(3);

            $ket = array(
                'Terima kasih atas kepercayaan anda',
                'Purchase order akan di terima jika sudah melalukan full payment',
                'Simpan bukti purchase order ini sebagai bukti yang SAH.',
                'Pembayaran di lakukan ke Account Rek BCA 4140474440 a/n HARYONO'
            );

            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(99, 10, 'Keterangan', 'B', 1);
            $pdf->Ln(2);
            $pdf->SetFont('Arial', '', 12);
            $pdf->Cell(99, 5, $ket[0], 0, 1);
            $pdf->Ln();

            $pdf->SetWidths(array(6, 93));
            $pdf->SetLineHeight(5);
            $pdf->SetAligns(array('', ''));
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(99, 10, 'Syarat & Ketentuan', 'B', 1);
            $pdf->Ln(2);

            $pdf->SetFont('Arial', 'I', 12);
            $pdf->Cell(99, 5, 'Paymet Metode ;', 0, 1);
            $pdf->Ln(2);
            $pdf->RowN(array('1.', $ket[1]));
            $pdf->RowN(array('2.', $ket[2]));
            $pdf->RowN(array('3.', $ket[3]));
            $pdf->Ln();

            $pdf->SetFont('Arial', '', 12);
            $date = date_create($b['tgl_']);
            $pdf->Cell(130, 5, '', 0, 0);
            $pdf->Cell(25, 5, date_format($date, "j M, Y"), 0, 1, 'C');
            $pdf->Ln(20);
            $pdf->Cell(130, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Finance', 0, 1, 'C'); //end of line
        }

        $file_name = $id;
        $pdf->Output('I', $file_name);
    }

    public function toggle($getId)
    {
        $id = encode_php_tags($getId);
        $invoice = $this->admin->get('po_invoice', ['no_inv' => $id]);
        $status = $invoice['status'];
        $toggle = $status ? 0 : 1;
        $pesan = $toggle ? 'Lunas.' : 'Piutang.';
        if ($toggle == 0) {
            $tgl = '';
        } else {
            $tgl = date('Y-m-d');
        }
        if ($this->admin->update('po_invoice', 'no_inv', $id, ['status' => $toggle, 'tgl_lns' => $tgl])) {
            echo $this->db->last_query();
            set_pesan($pesan);
        }
        redirect('invoice');
    }

    // coba cetak
    public function cetak($id)
    {
        $data['title'] = "INVOICE|$id";
        $data['page'] = 'size:  21cm 29.7cm;';
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['datainv'] = $this->admin->getPoInvoice(null, $id, null);
        $data['datainv_d'] = $this->admin->getPoInvoiceD(null, $id);
        $this->template->load('print', 'po_invoice/cetak', $data);
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchaseorder extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = "PURCHASE ORDER";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['datapo'] = $this->admin->getPurchaseorder();
        $this->template->load('templates/dashboard', 'purchase_order/data_po', $data);
    }

    // page input po
    public function add()
    {
        $data['title'] = "Purchase Order";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['supplier'] = $this->admin->get('supplier');
        $kode = 'PO-' . date('Y-');
        $kode_terakhir = $this->admin->getMax('purchase_order', 'no_po', $kode);
        $kode_tambah = substr($kode_terakhir, -4, 4);
        $kode_tambah++;
        $number = str_pad($kode_tambah, 4, '0', STR_PAD_LEFT);
        $data['no_po'] = $kode . $number;
        $data['purchase_order_d'] = $this->admin->getPurchaseorderD(null, $data['no_po']);
        $this->template->load('templates/dashboard', 'purchase_order/add_po', $data);
    }

    // page add detail po
    public function add_d()
    {
        $this->form_validation->set_rules('id_barang_po', 'Barang', 'required');
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
        $this->form_validation->set_rules('qty', 'Kuantitas', 'required|trim|numeric|greater_than[0]');
        $this->form_validation->set_rules('harga_d', 'Harga', 'required|trim|numeric');
        $this->form_validation->set_rules('diskon', 'Diskon', 'required|trim|numeric');
        $this->form_validation->set_rules('pajak_d', 'Pajak', 'required|trim|numeric');
        $this->form_validation->set_rules('total', 'Total', 'required|trim|numeric');
        if ($this->form_validation->run() == false) {
            $data['title'] = "Purchase Order Detail";
            $data['about'] = $this->admin->get('about', ['id' => '1']);
            $data['barang'] = $this->admin->getBarang();
            $kode = 'PO-' . date('Y-');
            $kode_terakhir = $this->admin->getMax('purchase_order', 'no_po', $kode);
            $kode_tambah = substr($kode_terakhir, -4, 4);
            $kode_tambah++;
            $number = str_pad($kode_tambah, 4, '0', STR_PAD_LEFT);
            $data['no_po'] = $kode . $number;

            $this->template->load('templates/dashboard', 'purchase_order/add_pod', $data);
        } else {
            $input = $this->input->post(null, true);
            $dataIn = array(
                'd_po' => $input['d_po'],
                'id_barang_po' => $input['id_barang_po'],
                'qty' => $input['qty'],
                'harga_d' => $input['harga_d'],
                'diskon' => $input['diskon'],
                'pajak_d' => $input['pajak_d'],
                'total' => $input['total']
            );
            $insert = $this->admin->insert('purchase_order_d', $dataIn);
            if ($insert) {
                set_pesan('Add barang berhasil.');
                redirect('purchaseorder/add');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('purchaseorder/add_d');
            }
        }
    }

    // proses insert di page input
    public function add_p()
    {
        $this->form_validation->set_rules('tanggal_', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('tanggal_brgmasuk', 'Tanggal Barang Masuk', 'required|trim');
        $this->form_validation->set_rules('supplier_id', 'Supplier', 'required|trim');
        $this->form_validation->set_rules('catatan', 'Catatan', 'required|trim');
        $this->form_validation->set_rules('subtotal', 'Sub Total', 'required|trim');
        $this->form_validation->set_rules('jumlah_tertagih', 'Jumlah Tertagih', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->add();
        } else {
            $input = $this->input->post(null, true);
            $insert = $this->admin->insert('purchase_order', $input);
            echo $this->db->last_query();
            if ($insert) {
                set_pesan('data berhasil disimpan.');
                redirect('purchaseorder');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('purchaseorder/add');
            }
        }
    }

    // get data barang untuk dropdown
    public function getBrg($id)
    {
        $data = $this->admin->getBarangWhere($id);
        echo json_encode($data);
    }

    // validasi detail po
    private function _validasi_d()
    {
        $this->form_validation->set_rules('namabarang', 'Produk', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim');
        $this->form_validation->set_rules('qty', 'Quantity', 'required|trim|numeric|greater_than[0]');
        $this->form_validation->set_rules('harga_d', 'Harga', 'required|trim|numeric|greater_than_equal_to[0]');
        $this->form_validation->set_rules('diskon', 'Diskon', 'required|trim|numeric|greater_than_equal_to[0]');
        $this->form_validation->set_rules('pajak_d', 'Pajak', 'required|trim|numeric|greater_than_equal_to[0]');
    }
    // proses detail po
    public function proses_d()
    {
        $this->_validasi_d();
        if ($this->form_validation->run() == false) {
            $this->add();
        } else {
            $input = $this->input->post(null, true);
            $insert = $this->admin->insert('purchase_order_d', $input);

            if ($insert) {
                // set_pesan('data berhasil disimpan.');
                redirect('purchaseorder/add');
            } else {
                // set_pesan('Opps ada kesalahan!');
                redirect('purchaseorder/add');
            }
        }
    }

    public function delete($getId)
    {
        $id = encode_php_tags($getId);
        if ($this->admin->delete('purchase_order', 'no_po', $id)) {
            $this->admin->delete('purchase_order_d', 'd_po', $id);
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('purchaseorder');
    }

    public function delete_d($getId)
    {
        $id = encode_php_tags($getId);
        if ($this->admin->delete('purchase_order_d', 'idpo', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('purchaseorder/add');
    }

    public function print_($id)
    {
        // get data table
        $data = $this->admin->getPurchaseorder(null, $id);
        $dat2 = $this->admin->getPurchaseorderD(null, $id);

        $this->load->library('PDF_MC_Table');

        $pdf = new PDF_MC_Table();
        $pdf->AddPage('P', 'A4');

        $pdf->SetFont('Arial', 'B', 14);

        $pdf->Cell(115, 5, ' ', 0, 0);
        $pdf->Image('assets/img/logosksejahtera.jpeg', 10, 10, 30);
        $pdf->Cell(74, 5, 'Purchase Order', 0, 1, 'R'); //end of line
        $pdf->Ln(3);

        $pdf->SetFont('Arial', '', 12);

        foreach ($data as $d) {
            $noinv = explode('-', $d['no_po']);
            $nama = explode(':', $d['data']);

            $nm = array('CV. Ryo Multi Makmur', $nama[0]);
            $al = array('Jln Kali Baru Barat 2 A NO:23 RT 003/010, ' . "\n" . 'Kel: Kali Baru, Kec: Cilincing Jakarta Utara, ' . "\n" . 'Kota Jakarta Utara, DKI Jakarta, Indonesia', $nama[1]);
            $tp = array('Telp: +62 858-1352-7166', $nama[2]);
            $em = array('Email: rmmsupport@sksejahtera.com', $nama[3]);

            $pdf->Cell(115, 5, ' ', 0, 0);
            $pdf->Cell(36, 5, 'Referensi', 0, 0, 'R');
            $pdf->Cell(38, 5, $noinv[0] . '/' . $noinv[1] . '/' . $noinv[2], 0, 1, 'R'); //end of line

            $pdf->Cell(115, 5, ' ', 0, 0);
            $pdf->Cell(36, 5, 'Tanggal', 0, 0, 'R');
            $date = date_create($d['tanggal_']);
            $pdf->Cell(38, 5, date_format($date, "d/m/Y"), 0, 1, 'R'); //end of line

            $pdf->Cell(115, 5, ' ', 0, 0);
            $pdf->Cell(36, 5, 'Status', 0, 0, 'R');
            if ($d['status'] == 0) {
                $st = 'On Process';
            } else {
                $st = 'Confirmed';
            }
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(38, 5, $st, 0, 1, 'R'); //end of line

            $pdf->SetFont('Arial', '', 12);
            $pdf->Cell(115, 5, ' ', 0, 0);
            $pdf->Cell(36, 5, 'No.NPWP', 0, 0, 'R');
            $pdf->Cell(38, 5, '355391301416000', 0, 1, 'R'); //end of line
            $pdf->Ln(4);
            $pdf->Line(10, 41, 199, 41);

            $pdf->SetWidths(array(99, 90));
            $pdf->SetLineHeight(5);
            $pdf->SetAligns(array('', ''));
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(99, 5, 'Info Perusahaan', 0, 0);
            $pdf->Cell(90, 5, 'Tagihan Untuk', 0, 1); //end of line
            $pdf->Ln(3);
            $pdf->Line(10, 48, 199, 48);

            $pdf->SetFont('Arial', '', 12);
            $pdf->RowN(array($nm[0], $nm[1]));
            $pdf->RowN(array($al[0], $al[1]));
            $pdf->RowN(array($tp[0], 'Telp: ' . $tp[1]));
            $pdf->RowN(array($em[0], 'Email: ' . $em[1]));
            $pdf->Ln();
        }

        $pdf->SetWidths(array(65, 15, 25, 15, 25, 44));
        $pdf->SetLineHeight(5);
        $pdf->SetAligns(array('', 'R', 'R', 'R', 'C', 'R'));
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(65, 5, 'Produk', 1, 0);
        $pdf->Cell(15, 5, 'Qty', 1, 0, 'C');
        $pdf->Cell(25, 5, 'Harga', 1, 0, 'C');
        $pdf->Cell(15, 5, 'Disc', 1, 0, 'C');
        $pdf->Cell(25, 5, 'Pajak', 1, 0, 'C');
        $pdf->Cell(44, 5, 'Jumlah', 1, 1, 'C'); //end of line

        $pdf->SetFont('Arial', '', 12);
        foreach ($dat2 as $v) {
            $pdf->Row(array(
                $v['nama_barang'] . ' - ' . $v['deskripsi'],
                $v['qty'],
                number_format($v['harga_d'], 2, ',', '.'),
                $v['diskon'] . ' %',
                $v['pajak_d'] . ' %',
                number_format($v['jumlah_d'], 2, ',', '.')
            ));
        }
        $pdf->Ln();

        foreach ($data as $b) {
            //summary
            $pdf->Cell(94, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Subtotal', 0, 0, 'R');
            $pdf->Cell(70, 5, 'Rp ' . number_format($b['subtotal'], 2, ',', '.'), 0, 1, 'R'); //end of line

            $pdf->Cell(94, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Total Diskon', 0, 0, 'R');
            $pdf->Cell(70, 5, 'Rp ' . '(' . number_format($b['total_diskon'], 2, ',', '.') . ')', 0, 1, 'R'); //end of line

            $pdf->Cell(94, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Diskon Tambahan', 0, 0, 'R');
            $pdf->Cell(70, 5, 'Rp ' . number_format($b['diskon_tambah'] / 100 * $b['subtotal'], 2, ',', '.'), 0, 1, 'R'); //end of line

            $pdf->Cell(94, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Pajak', 0, 0, 'R');
            $pdf->Cell(70, 5, 'Rp ' . number_format($b['pajak'], 2, ',', '.'), 0, 1, 'R'); //end of line

            $pdf->Cell(94, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Total', 0, 0, 'R');
            $pdf->Cell(70, 5, 'Rp ' . number_format($b['total'], 2, ',', '.'), 0, 1, 'R'); //end of line

            $pdf->Cell(74, 5, '', 0, 0);
            $pdf->Cell(45, 7, 'Pajak Inclusive ' . $b['pajak_inc'] . '%', 'B', 0, 'R');
            $cc = ((100 / (100 + $b['pajak_inc'])) * $b['total']) * $b['pajak_inc'] / 100;
            $pdf->Cell(70, 7, 'Rp ' . number_format($cc, 2, ',', '.'), 'B', 1, 'R'); //end of line

            $pdf->Ln(2);
            $pdf->Cell(94, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Jumlah Tertagih:', 0, 0, 'R');
            $pdf->Cell(70, 5, 'Rp ' . number_format($b['jumlah_tagihan'], 2, ',', '.'), 0, 1, 'R'); //end of line
            $pdf->Ln(3);

            $ket = array(
                'Terima kasih atas kepercayaan anda',
                'Purchase order akan di terima jika sudah melalukan full payment',
                'Simpan bukti purchase order ini sebagai bukti yang SAH.',
                'Pembayaran di lakukan ke Account Rek BCA 4140474440 a/n HARYONO'
            );

            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(99, 10, 'Keterangan', 'B', 1);
            $pdf->Ln(2);
            $pdf->SetFont('Arial', '', 12);
            $pdf->Cell(99, 5, $ket[0], 0, 1);
            $pdf->Ln();

            $pdf->SetWidths(array(6, 93));
            $pdf->SetLineHeight(5);
            $pdf->SetAligns(array('', ''));
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(99, 10, 'Syarat & Ketentuan', 'B', 1);
            $pdf->Ln(2);

            $pdf->SetFont('Arial', 'I', 12);
            $pdf->Cell(99, 5, 'Paymet Metode ;', 0, 1);
            $pdf->Ln(2);
            $pdf->RowN(array('1.', $ket[1]));
            $pdf->RowN(array('2.', $ket[2]));
            $pdf->RowN(array('3.', $ket[3]));
            $pdf->Ln();

            $pdf->SetFont('Arial', '', 12);
            $date = date_create($b['tanggal_']);
            $pdf->Cell(130, 5, '', 0, 0);
            $pdf->Cell(25, 5, date_format($date, "j M, Y"), 0, 1, 'C');
            $pdf->Ln(20);
            $pdf->Cell(130, 5, '', 0, 0);
            $pdf->Cell(25, 5, 'Finance', 0, 1, 'C'); //end of line
        }

        $file_name = $id;
        $pdf->Output('I', $file_name);
    }

    public function toggle($getId)
    {
        $id = encode_php_tags($getId);
        $status = $this->admin->get('purchase_order', ['no_po' => $id])['status'];
        $toggle = $status ? 0 : 1; //Jika user aktif maka nonaktifkan, begitu pula sebaliknya
        $pesan = $toggle ? 'po confirmed.' : 'po on process.';
        if ($toggle == 0) {
            $tgl = '';
        } else {
            $tgl = date('Y-m-d');
        }
        if ($this->admin->update('purchase_order', 'no_po', $id, ['status' => $toggle, 'tanggal_brgmasuk' => $tgl])) {
            set_pesan($pesan);
        }
        redirect('purchaseorder');
    }

    // supplier add from po
    public function add_sup()
    {
        $this->form_validation->set_rules('nama_supplier', 'Nama Supplier', 'required|trim');
        $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|trim|numeric');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
        $this->form_validation->set_rules('nm_bank', 'Nama Bank', 'required|trim');
        $this->form_validation->set_rules('no_rek', 'Nomor Rekening', 'required|trim|numeric');
        if ($this->form_validation->run() == false) {
            $data['title'] = "Supplier";
            $data['about'] = $this->admin->get('about', ['id' => '1']);
            $this->template->load('templates/dashboard', 'purchase_order/add_sup', $data);
        } else {
            $input = $this->input->post(null, true);
            $save = $this->admin->insert('supplier', $input);
            if ($save) {
                // set_pesan('data berhasil disimpan.');
                redirect('purchaseorder/add');
            } else {
                set_pesan('data gagal disimpan', false);
                redirect('purchaseorder/add_sup');
            }
        }
    }

    // coba cetak
    public function cetak($id)
    {
        $data['title'] = "PO|$id";
        $data['page'] = 'size:  21cm 29.7cm;';
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['datapo'] = $this->admin->getPurchaseorder(null, $id);
        $data['datapo_d'] = $this->admin->getPurchaseorderD(null, $id);
        $this->template->load('print', 'purchase_order/cetak', $data);
    }
}

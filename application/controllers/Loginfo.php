<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Loginfo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();
        if (!is_owner()) {
            redirect('dashboard');
        }

        $this->load->model('Admin_model', 'admin');
        $this->load->library('form_validation');

        $userId = $this->session->userdata('login_session')['user'];
        // $this->user = $this->admin->get('user', ['id_user' => $userId]);
    }

    public function index()
    {
        $data['title'] = "History Log In-Out";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['log_user'] = $this->admin->getLog();
        $this->template->load('templates/dashboard', 'loginfo', $data);
    }
}

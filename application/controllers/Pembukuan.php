<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembukuan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = "Pembukuan";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['po_invoice'] = $this->admin->count('po_invoice');
        $data['purchase_order'] = $this->admin->count('purchase_order');
        $data['tandaterima'] = $this->admin->count('tandaterima');

        $this->template->load('templates/dashboard', 'pembukuan/data', $data);
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barangwest extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = "Barang West";
        $data['about'] = $this->admin->get('about', ['id' => '1']);
        $data['barangwest'] = $this->admin->getBarangwest();
        $this->template->load('templates/dashboard', 'barang_west/data', $data);
    }

    private function _validasi()
    {
        $this->form_validation->set_rules('tanggal_west', 'Tanggal West', 'required|trim');
        $this->form_validation->set_rules('subtotal', 'Sub Total', 'required|trim|numeric');
    }

    public function add()
    {
        $data['title'] = "Barang Sisa";
        $data['about'] = $this->admin->get('about', ['id' => '1']);

        // Mendapatkan dan men-generate kode transaksi barang west
        $kode = 'BW' . userdata('kd_user') . '-' . date('ymd');
        $kode_terakhir = $this->admin->getMax('barang_west', 'id_barang_west', $kode);
        $kode_tambah = substr($kode_terakhir, -5, 5);
        $kode_tambah++;
        $number = str_pad($kode_tambah, 5, '0', STR_PAD_LEFT);
        $data['id_barang_west'] = $kode . $number;
        $data['barangwest_d'] = $this->admin->getBWdetail($data['id_barang_west']);
        $this->template->load('templates/dashboard', 'barang_west/add2', $data);
    }

    public function add_p()
    {
        $input = $this->input->post(null, true);
        $insert = $this->admin->insert('barang_west', $input);

        if ($insert) {
            set_pesan('data berhasil disimpan.');
            redirect('barangwest');
        } else {
            set_pesan('Opps ada kesalahan!');
            redirect('barangwest/add');
        }
    }

    public function add_d()
    {
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required|trim');
        $this->form_validation->set_rules('harga', 'Harga', 'required|trim|numeric');
        $this->form_validation->set_rules('jumlah_west', 'Jumlah West', 'required|trim|numeric');

        if ($this->form_validation->run() == false) {
            $data['title'] = "Barang Sisa";
            $data['about'] = $this->admin->get('about', ['id' => '1']);
            // $data['barangwest_d'] = $this->admin->getBWdetail('barang_west_d');
            // Mendapatkan dan men-generate kode transaksi barang west
            $kode = 'BW' . userdata('kd_user') . '-' . date('ymd');
            $kode_terakhir = $this->admin->getMax('barang_west', 'id_barang_west', $kode);
            $kode_tambah = substr($kode_terakhir, -5, 5);
            $kode_tambah++;
            $number = str_pad($kode_tambah, 5, '0', STR_PAD_LEFT);
            $data['id_barang_west'] = $kode . $number;

            $this->template->load('templates/dashboard', 'barang_west/add_d', $data);
        } else {
            $input = $this->input->post(null, true);
            $insert = $this->admin->insert('barang_west_d', $input);
            echo $this->db->last_query();

            if ($insert) {
                set_pesan('Add barang berhasil.');
                redirect('barangwest/add');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('barangwest/add_d');
            }
        }
    }

    public function delete($getId)
    {
        $id = encode_php_tags($getId);
        if ($this->admin->delete('barang_west', 'id_barang_west', $id)) {
            $this->admin->delete('barang_west_d', 'id_barang_west', $id);
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('barangwest');
    }

    public function delete_d($getId)
    {
        $id = encode_php_tags($getId);
        if ($this->admin->delete('barang_west_d', 'id', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('barangwest/add');
    }
}
